var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var multer = require('multer');
var mime = require('mime');

var query = "SELECT id, role_id FROM users";

var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './public/uploads');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now() + '.'+mime.extension(file.mimetype));
  }
});
var upload = multer({ storage : storage}).single('upload');

var dbconn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'conference_poc_db'
});

dbconn.connect(function (err) {
    if (err) {
        console.log('Database connection error');
    } else {

        console.log('Database connection successful')
    }
});


dbconn.query(query, function (err, result) {
    if (err) {
        console.log(err);
    }
    else {
        //console.log(result);
    }
});



new CronJob('100 * * * * *', function ()
{
    console.log('You will see this message every hour');
    var query = "UPDATE conference SET status = 1 WHERE end_date < now() AND status != 2";
    dbconn.query(query,
        function (err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log("Update Result",result);
                query = "SELECT * FROM conference WHERE status = 1";
                dbconn.query(query,
                    function (err, result) {
                        if (err) {
                            console.log("Conference ERROR",err);
                        } else {
                            console.log("Conference Result",result);
                            var conferences = [];
                            conferences = result;

                            for (var i = 0; i < conferences.length; i++)
                            {

                                var cId = conferences[i].id;

                                query = "SELECT * FROM user_conference WHERE conference_id = " + cId;
                                dbconn.query(query,
                                    function (err, result) {
                                        if (err) {
                                            console.log("user_conference Error",err);
                                        } else {
                                            console.log("user_conference Result",result);

                                            var usersConferences = [];
                                            usersConferences = result;

                                            var count = usersConferences.length;
                                            query = "SELECT * FROM activities WHERE conference_id = " + cId;

                                            dbconn.query(query,
                                                function (err, result) {
                                                    if (err) {
                                                        console.log(err);
                                                    } else {
                                                        console.log("activities result",result);

                                                        var activities = [];
                                                        activities = result;

                                                        for (var j = 0; j < activities.length; j++) {

                                                            var aId = activities[j].id;
                                                            var userId;
                                                            if (count > 0)
                                                            {
                                                                count--;
                                                                userId = usersConferences[count].users_id;

                                                                query = "INSERT INTO reviews (activity_id, user_id) VALUES (" + aId + ", " + userId + ")";
                                                                console.log("reviews",query);
                                                                console.log("Activity ID",aId);
                                                                console.log("UserID",userId);
                                                                dbconn.query(query,
                                                                    function (err, result) {
                                                                        if (err) {
                                                                            console.log("reviews  ERROR",err);
                                                                        } else {
                                                                            console.log("reviews  ERROR",result);
                                                                        }
                                                                    });

                                                            }
                                                        }


                                                        query = "UPDATE conference SET status = 2 WHERE id = " + cId;
                                                        dbconn.query(query,
                                                            function (err, result) {
                                                                if (err) {
                                                                    console.log(err);
                                                                } else {
                                                                    //console.log(result);
                                                                }
                                                            });
                                                    }
                                                });
                                        }
                                    });
                            }
                        }
                    });
            }
        });

    var query = "SELECT id,review_days,end_date FROM conference";
    dbconn.query(query,
        function (err, result) {
            if (err) {
                console.log(err);
            } else {
                var revArray = [];
                revArray = result;

                for (var i = 0; i < revArray.length; i++) {
                    var reviewDays = revArray[i].review_days;
                    var conferenceId = revArray[i].id;
                    var endDate = revArray[i].end_date;

                    //console.log(reviewDays, conferenceId, endDate);

                    var query = "UPDATE conference SET status = 3 WHERE end_date + INTERVAL " + reviewDays + " DAY < CURDATE() AND id = " + conferenceId;
                    updateStatus(query);
                }
            }
        });
}, null, true, 'America/Los_Angeles');




/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/register',function (req,res) {


    res.header('Access-Control-Allow-Origin: *');
    res.header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

    console.log(req.body);

    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var age = req.body.age;
    var gender = req.body.gender;
    var email  = req.body.email;
    var password = req.body.password;
    var roleID = req.body.role_id;
    var status = 1;



    var insertQuery = "INSERT INTO users (first_name,last_name,age,gender,email,password,role_id,status) VALUES ('" + firstName + "','" + lastName + "'," + age + "," + gender + ",'" + email + "','" + password + "'," + roleID + "," + status + ")";
    console.log(insertQuery);


    dbconn.query(insertQuery,
        function (err, result) {
            // Case there is an error during the creation
            if (err) {
                console.log(err);

                var responseJson = new Object();
                responseJson.data = "";
                responseJson.success = false;
                responseJson.message = "Email Id already registered.";
                res.send(responseJson);
            } else
            {

                var selectQuery = "SELECT * FROM users WHERE id = " + result.insertId;
                dbconn.query(selectQuery,function (err, result) {

                    if(err)
                    {
                        console.log(err);
                    }else
                    {
                        console.log(result);
                        var responseJson = new Object();
                        responseJson.data = result;
                        responseJson.success = true;
                        responseJson.message = "";
                        res.send(responseJson);
                    }

                });
                

            }
        });

});


router.post('/signin',function (req,res) {

    console.log(req.body);

    var email = req.body.email;
    var password = req.body.password;

    var query = "SELECT * FROM users WHERE email = '" + email + "' AND password = '" + password +"'";

    dbconn.query(query,function (err,result) {

        if(err)
        {
            console.log(err);
        }else
        {
            var userArray =[];
            var userArray = result;

            if (userArray != null && userArray.length > 0) {
                var responseJson = new Object();
                responseJson.data = userArray[0];
                responseJson.success = true;
                responseJson.message = "successful";
                res.send(responseJson);
            } else {
                var responseJson = new Object();
                responseJson.data = null;
                responseJson.success = false;
                responseJson.message = "user not found";
                res.send(responseJson);
            }
        }

    });
});


router.post('/createConference',function (req,res) {

    var conferenceName = req.body.name;
    var description = req.body.description;
    var startDate = req.body.start_date;
    var endDate = req.body.end_date;
    var reviewDays = req.body.review_days;
    var status = req.body.status;

											  
    var query = "INSERT INTO conference (name,description,start_date,end_date,review_days,status) VALUES ('" + conferenceName + "','" + description + "','" + startDate + "','" + endDate + "'," + reviewDays + "," + status + ")";

	console.log(query);
    dbconn.query(query,function (err,result) {

        if(err)
        {
            console.log(err);
        }else
        {
            var obj = new Object();
            obj.conference_id = result.insertId;

            var responseJson = new Object();
            responseJson.data = obj;
            responseJson.success = true;
            responseJson.message = "successful";
            res.send(responseJson);
        }

    });

});

router.post('/joinConference',function (req,res) {

    console.log(req.body);

    var userID = req.body.user_id;
    var conferenceID = req.body.conference_id;


    var query = "INSERT INTO user_conference (users_id,conference_id) VALUES (" + userID + "," + conferenceID + ")";
    dbconn.query(query,function (err,result) {

        if(err)
        {
            console.log(err);

        }else
        {
            var responseJson = new Object();
            responseJson.data = "";
            responseJson.success = true;
            responseJson.message = "successful";
            res.send(responseJson);
        }
    });



});

router.post('/getAllConferences',function (req,res) {

    console.log(req.body);
    var userID = req.body.user_id;

    var query = "SELECT conference_id FROM user_conference WHERE users_id =" + userID;
	console.log(query);
    dbconn.query(query,function (err,result) {

        if(err)
        {
            console.log(err);
        }else
        {
            var conferenceIdsArray = [];
            var string = JSON.stringify(result);
            var json = JSON.parse(string);

            for (var i=0; i<json.length; i++)
            {
                conferenceIdsArray.push(json[i].conference_id);
            }

            var conferenceIdsString = "(" +conferenceIdsArray.join() + ")";

			var confQuery = ""
			if(conferenceIdsArray.length > 0){
			confQuery = "SELECT * FROM conference WHERE id NOT IN " + conferenceIdsString + " AND status != 0";	
			}else{
			confQuery = "SELECT * FROM conference WHERE status != 0";	
			}
            
            
			console.log(confQuery);
			dbconn.query(confQuery,function (err,result) {

                if(err)
                {
                    console.log(err);
                }else
                {
                    console.log(result);
                    var responseJson = new Object();
                    responseJson.data = result;
                    responseJson.success = true;
                    responseJson.message = "successful";
                    res.send(responseJson);
                }
            });



        }

    });

});


router.post('/getMyConference',function (req,res) {

    var userID = req.body.user_id;
    var status = req.body.state;

    var query = "SELECT conference_id FROM user_conference WHERE users_id =" + userID;
    dbconn.query(query,function (err,result) {

        if (err) {
            console.log(err);
        } else {
            var conferenceIdsArray = [];
            var string = JSON.stringify(result);
            var json = JSON.parse(string);

            for (var i = 0; i < json.length; i++)
            {
                conferenceIdsArray.push(json[i].conference_id);
            }

            var conferenceIdsString = "(" +conferenceIdsArray.join() + ")";

            var confQuery = "SELECT * FROM conference WHERE id IN " + conferenceIdsString + "AND status = " + status;
            dbconn.query(confQuery,function (err,result) {

                if(err)
                {
                    console.log(err);
                }else
                {
                    var responseJson = new Object();
                    responseJson.data = result;
                    responseJson.success = true;
                    responseJson.message = "successful";
                    res.send(responseJson);
                }
            });
        }
    });

});


router.post('/submitDocument',function (req,res) {

    console.log(req.body);

    var path = req.body.path;
    var documentName = req.body.documentName;
    var description = req.body.description;
    var conferenceID = req.body.conferenceID;
    var userID = req.body.userID;
    var roleID = req.body.roleID;
    var comments = req.body.comments;


    var query = "INSERT INTO documents (path,documentName,description) VALUES ('" + path + "','" + documentName + "','" + description + "')";
    console.log(query);
    dbconn.query(query,function (err,result) {

        if(err)
        {
            console.log(err);
        }else
        {
            console.log(result);
            var documentID = result.insertId;
			 var conferenceID = req.body.conferenceID;
    		var userID = req.body.userID;
    		var roleID = req.body.roleID;
    		var comments = req.body.comments;
            var activityQuery = "INSERT INTO activities (conference_id,user_id,role_id,doc_id,comments) VALUES (" + conferenceID + "," + userID + "," + roleID + "," + documentID + ",'" + comments + "')";
            console.log(activityQuery);
            dbconn.query(activityQuery,function (err,result) {

                if(err)
                {
                    console.log(err);
					var responseJson = new Object();
                    responseJson.data = err;
                    responseJson.success = false;
                    responseJson.message = "error";
                    res.send(responseJson);
                }else
                {
                    console.log(result);
					
					var responseJson = new Object();
                    responseJson.data = "";
                    responseJson.success = true;
                    responseJson.message = "Document Submitted";
                    res.send(responseJson);
                }

            });

        }

    });

});


router.post('/getAllConferencesAdmin',function (req,res) {

    console.log(req.body);
    var userID = req.body.user_id;

    var query = "SELECT * FROM `conference`";
	console.log(query);
   
			dbconn.query(query,function (err,result) {

                if(err)
                {
                    console.log(err);
					 var responseJson = new Object();
                    responseJson.data = err;
                    responseJson.success = false;
                    responseJson.message = "error";
                    res.send(responseJson);
					
                }else
                {
                    console.log(result);
                    var responseJson = new Object();
                    responseJson.data = result;
                    responseJson.success = true;
                    responseJson.message = "successful";
                    res.send(responseJson);
                }
            });
	
	
	
});



router.post('/getConferenceDetails',function (req,res) {

    console.log(req.body);
    var conf_id = req.body.conf_id;

    var query = "SELECT * FROM conference where id = " + conf_id;
	console.log(query);
   
			dbconn.query(query,function (err,result) {

                if(err)
                {
                    console.log(err);
					 var responseJson = new Object();
                    responseJson.data = err;
                    responseJson.success = false;
                    responseJson.message = "error";
                    res.send(responseJson);
					
                }else
                {
                    console.log(result);
                    var responseJson = new Object();
                    responseJson.data = result;
                    responseJson.success = true;
                    responseJson.message = "successful";
                    res.send(responseJson);
                }
            });
	
	
	
});



router.post('/getConfrencesAgainstId',function (req,res) {

    var conferenceID = req.body.conference_id;
   	var user_id = req.body.user_id;
	var query = "SELECT doc_id FROM activities WHERE conference_id = " + conferenceID;
    console.log(query);

    dbconn.query(query,function (err,result) {

        if(err)
        {

        }else
        {
            console.log(result);
            var docIdsArray = [];
            var string = JSON.stringify(result);
            var json = JSON.parse(string);

            for (var i = 0; i < json.length; i++)
            {
                docIdsArray.push(json[i].doc_id);
            }

            var docIdsString = "(" +docIdsArray.join() + ")";
            console.log(docIdsString);

            // var confQuery = "SELECT * FROM conference WHERE id IN " + conferenceIdsString + "AND status = " + status;


            var docQuery = "SELECT * FROM documents WHERE id IN " +  docIdsString;
            dbconn.query(docQuery,function (err,result) {

                if(err)
                {
                    var responseJson = new Object();
                    responseJson.data = null;
                    responseJson.success = false;
                    responseJson.message = "user not found";
                    res.send(responseJson);
                }else
                {

                    if (result != null && result.length > 0)
                    {
                        console.log(result);
                        var responseJson = new Object();
                        responseJson.data = result;
                        responseJson.success = true;
                        responseJson.message = "successful";
                        res.send(responseJson);
                    }else
                    {
                        var responseJson = new Object();
                        responseJson.data = null;
                        responseJson.success = false;
                        responseJson.message = "user not found";
                        res.send(responseJson);
                    }


                }
            });


        }
    });

});



router.post('/getAllUsers',function (req,res) {

    var query = "SELECT * FROM users where role_id != 0";
	console.log(query);
	dbconn.query(query,function (err,result) {

                if(err)
                {
                    console.log(err);
                }else
                {
                    console.log(result);
                    var responseJson = new Object();
                    responseJson.data = result;
                    responseJson.success = true;
                    responseJson.message = "successful";
                    res.send(responseJson);
                }
            });


});



router.post('/updateUser',function (req,res) {

	/**
	  public id:number;
  public first_name:string;
  public last_name:string;
  public age:number;
  public gender:number;
  public email:string;
  public password:string;
  public role_id:number;
  public status:number;*/
	
	
    console.log(req.body);
	var status_id = req.body.status_id;
	var user_id = req.body.user_id;
	
	var query = "Update users set STATUS = " + status_id + " where id = " + user_id;

	
   // var query = "SELECT * FROM users WHERE email = '" + email + "' AND password = '" + password +"'";

    dbconn.query(query,function (err,result) {

			if(err)
                {
                    console.log(err);
					var responseJson = new Object();
                    responseJson.data = err;
                    responseJson.success = false;
                    responseJson.message = "error";
                    res.send(responseJson);
                }else
                {
                    console.log(result);
					
					var responseJson = new Object();
                    responseJson.data = "";
                    responseJson.success = true;
                    responseJson.message = "Role Updated";
                    res.send(responseJson);
                }

    });
});


router.post('/upload',function(req,res){
    upload(req,res,function(err) {
      /*  if(err) {
            return res.end("Error uploading file.");
        }
			//console.dir(req.file.filename);
         var hostname = req.headers.host; // hostname = 'localhost:8080'
		console.log('http://'+hostname+"/uploads/"+req.file.filename);
		
		res.end("File is uploaded" + res);
    	*/
		
		
		if(err){
                    console.log(err);
					var responseJson = new Object();
                    responseJson.data = err;
                    responseJson.success = false;
                    responseJson.message = "error";
                    res.send(responseJson);
          }else
                {
                    console.log(result);
					var filepath = 'http://'+req.headers.host+'/uploads/'+req.file.filename;
					var result = {
						"originalname":req.file.originalname,
						"path": filepath
					};
					var responseJson = new Object();
                    responseJson.data = result;
                    responseJson.success = true;
                    responseJson.message = "File uploaded";
                    res.send(responseJson);
                }

		
		
	});
});






function updateStatus(query) {
    dbconn.query(query,
        function (err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log(result);

            }
        });
}


router.post('/getAllReviews',function (req, res) {

    console.log(req.body);

    var userId = req.body.user_id;
    var isActive = req.body.is_active;
    var jsonObject = new Object();


	var query;

	if(isActive == 'true'){
		query = "Select r.id, c.name, u.first_name, u.last_name, d.path from reviews as r, activities as a, conference as c, users as u, documents as d where r.activity_id = a.id AND a.conference_id = c.id AND a.user_id = u.id AND a.doc_id = d.id AND r.user_id = "+  userId  + " AND c.status <= 2"
	}else{
		query = "Select r.id, c.name, u.first_name, u.last_name, d.path from reviews as r, activities as a, conference as c, users as u, documents as d where r.activity_id = a.id AND a.conference_id = c.id AND a.user_id = u.id AND a.doc_id = d.id AND r.user_id = "+  userId  + " AND c.status = 3"
	}
     

    // var query = "SELECT conference_id,doc_id FROM conference_poc_db.activities WHERE user_id= "+ userId;
    dbconn.query(query,function (err,result)
    {
        if(err)
        {
            console.log(err);
        }else
        {
            console.log(result);

            if(result.length == 0)
            {
                var responseJson = new Object();
                responseJson.data ="";
                responseJson.success = true;
                responseJson.message = "success";
                res.send(responseJson);
            }else
            {
                var responseJson = new Object();
                responseJson.data =result;
                responseJson.success = true;
                responseJson.message = "success";
                res.send(responseJson);
            }

        }
    });

});



router.post('/submitReview',function (req,res) {

    console.log('this is submit review ' + req.body);
    var reviewID = req.body.review_id;
    var docName = req.body.doc_name;
    var docPath = req.body.doc_path;
    var comments = req.body.comments;


    var query = "INSERT INTO documents (path,documentName,description) VALUES ('" + docPath + "','" + docName + "','" + comments + "')";
	
    dbconn.query(query,function (err,result) {

        if(err)
        {
            console.log(err);
        }else
        {

            console.log(result);
			var rID = req.body.review_id;
            var docID = result.insertId;
            var query = "UPDATE reviews SET doc_id = " + docID + " WHERE id = " + rID;
            console.log(query);
            dbconn.query(query,function (err,result) {

                if(err){

                    console.log(err);
                }else
                {
                    console.log(result);
                    var responseJson = new Object();
                    responseJson.data ="";
                    responseJson.success = true;
                    responseJson.message = "success";
                    res.send(responseJson);

                }
            });




        }
    });


});



router.post('/getConfrenceDetailsForUser',function (req,res) {

    var conferenceID = req.body.conference_id;
   	var user_id = req.body.user_id;
	var query = "SELECT `users`.`first_name`, `documents`.`documentName`,`documents`.`path`,`documents`.`id`, `user_conference`.`conference_id`, `activities`.`role_id`, `activities`.`comments`, `activities`.`doc_id`, `activities`.`user_id`, `activities`.`conference_id` FROM `user_conference` , `documents` , `users` , `activities` where users.id = "+ user_id +" and user_conference.conference_id = "+ conferenceID;
	
	
	
            dbconn.query(query,function (err,result) {

                if(err)
                {
					console.log(err);
                    var responseJson = new Object();
                    responseJson.data = null;
                    responseJson.success = false;
                    responseJson.message = "user not found";
                    res.send(responseJson);
                }else
                {

                    if (result != null && result.length > 0)
                    {
                        console.log(result);
                        var responseJson = new Object();
                        responseJson.data = result;
                        responseJson.success = true;
                        responseJson.message = "successful";
                        res.send(responseJson);
                    }else
                    {
                        var responseJson = new Object();
                        responseJson.data = null;
                        responseJson.success = false;
                        responseJson.message = "user not found";
                        res.send(responseJson);
                    }


                }
            });
	
	
	

});

/**

SELECT  `documents`.`documentName`,`documents`.`path`,`documents`.`id`, `user_conference`.`conference_id`, `activities`.`role_id`, `activities`.`comments`, `activities`.`doc_id`, `activities`.`user_id`, `activities`.`conference_id`
FROM `user_conference` , `documents` , `users` , `activities` where users.id = 2 and user_conference.conference_id = 1

*/


/**

SELECT UNIQUE `conference`.`name`, `conference`.`description`, `users`.`first_name`, `users`.`last_name`, `documents`.`path`, `documents`.`id` as DOCUMENT_ID,  `documents`.`documentName`, `reviews`.`doc_id` AS REVIEWS_DOC_ID, `reviews`.`id` AS REVIEW_ID FROM `user_conference` , `documents` , `users` , `activities` , `conference` , `reviews` where users.id = 2 AND conference.id = 4



*/

module.exports = router;
