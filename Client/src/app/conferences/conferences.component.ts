import { Component, OnInit,ElementRef } from '@angular/core';
import {RouterLink, ROUTER_DIRECTIVES, Router} from "@angular/router";
import {LoggedinComponent} from "../loggedin/loggedin.component";
import {Conference,Response} from '../models/index';
import {PocServiceService} from "../poc-service.service";
import {forEach} from "@angular/router/src/utils/collection";
@Component({
  moduleId: module.id,
  selector: 'app-conferences',
  templateUrl: 'conferences.component.html',
  styleUrls: ['conferences.component.css'],
  directives:[RouterLink,ROUTER_DIRECTIVES,LoggedinComponent],
  providers:[PocServiceService]
})
export class ConferencesComponent implements OnInit {

  items:Array<string>;
  searchResults:Array<string>;
  router:Router;
  constructor(router:Router,private service:PocServiceService) {
    this.router =  router;
    this.items = [];
    this.searchResults = [];
    this.getAllConferences();
  }

  ngOnInit(){
    // jQuery(this.elementRef.nativeElement.contains('#grid-basic')).bootgrid();
  }

  onKeyUp(value:string){
    console.log('key up' + value);

    var results = [];
    for (var i = 0; i < this.items.length; i++) {
      var conf = this.items[i];

      if (conf['name'].indexOf(value) == 0) {
        results.push(this.items[i]);
      }
    }
    this.searchResults = results;


  }

  onRowClick(user){
    var conf = this.searchResults[user.rowIndex - 1];
   console.dir(conf);
    this.router.navigate(['conferencedetails',conf['id']]);
  }

  onAddConferenceClick(){
    this.router.navigate(['addconference']);
  }




  getAllConferences(){
    this.service.getAllConferencesAdmin().subscribe(
      response => {
        // console.dir(response);
        this.processResponse(response);

      },
      error => {
        this.logError(error)
      },
      () => {
        console.log('Get All Conferences Complete')
      });
  }

  processResponse(response){


    var confResponse:Response = <Response>response;


    if(confResponse.success){
        var index = 0;

        for(;index < confResponse.data.length;index++){
          var conf = confResponse.data[index];
          console.dir(conf);
          this.items.push(conf);
        }
        this.searchResults = this.items;

    }else{
      this.searchResults = [];
    }


  }


  logError(error){
    console.error("Error in getAllConference() " + error);
  }


  getStatusForConference(value){

    if(value == 0){
      return 'Closed';
    }else if(value == 1){
      return 'Active';
    }else if(value == 2){
      return 'Open';
    }

  }


}
