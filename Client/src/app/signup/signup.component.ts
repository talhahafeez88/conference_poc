import { Component, OnInit } from '@angular/core';
import {ROUTER_DIRECTIVES,Router} from '@angular/router';
import {LoginComponent} from "../login/login.component";
import {Users} from '../models/index';
import {PocServiceService} from "../poc-service.service";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
@Component({
  moduleId: module.id,
  selector: 'app-signup',
  templateUrl: 'signup.component.html',
  styleUrls: ['signup.component.css'],
  directives:[ROUTER_DIRECTIVES,LoginComponent],
  providers:[PocServiceService]
})
export class SignupComponent implements OnInit {

  suser:Users;
  genders = ['Male','Female','Other'];
  roles = ['Reviewer','Author'];
  service:PocServiceService;
  email_error= true;

  constructor(private router:Router,service:PocServiceService) {

    this.suser = new Users("","");
    this.service = service;
    this.suser.gender=1;
    this.suser.role_id = 1;
//    this.setupUserForSignup();

  }


  setupUserForSignup(){
    this.suser.first_name = "Asad";
    this.suser.last_name  = "Rehman";
    this.suser.age = 29;
    this.suser.gender = 1;
    this.suser.email = "asad@one.com";
    this.suser.password = "root123";
    this.suser.role_id = 1;


  }
  ngOnInit() {
  }

  doSignup(f){
    console.log(f);
    this.email_error = false;
    this.email_error = true;
    var signupJson = this.getFormJSON();


    var result = this.service.doSignup(signupJson).
                      subscribe(
                        response =>
                        {
                          console.dir(response);
                         this.processSignupResults(response);
                        },
                        error =>{
                          this.errorInSignup(error)
                        },
                        () => {
                          console.log('Signup Complete')
                        }
    );

  }


  processSignupResults(response){

    if(!response['success']){
        this.email_error = false;
    }else{
      this.email_error = false;
      let responseUser:Users = <Users>response.data[0];

      if(responseUser.role_id == 0){
      }else if(responseUser.role_id == 1){
        this.doSignIn(this.suser.email,this.suser.password);
      }

    }

  }

  roleChanged(value){
      if(value == 'Reviewer'){
          this.suser.role_id = 1;
      }else{
          this.suser.role_id = 2;
      }

      console.log('role changed' + value);
  }

  genderChanged(value){
    if(value == 'Reviewer'){
      this.suser.role_id = 1;
    }else{
      this.suser.role_id = 2;
    }

    console.log('role changed' + value);
  }


  getFormJSON(){
    var me = this;
    var signupForm = {
      "first_name": me.suser.first_name,
      "last_name" : me.suser.last_name,
      "age"       : me.suser.age,
      "gender"    : me.suser.gender,
      "email"     : me.suser.email,
      "password"  : me.suser.password,
      "role_id"   : me.suser.role_id
    };


    console.log(signupForm);
    return signupForm;
  }



    errorInSignup(response){
        alert('An Error occured during signup. Please try again later');
    }




  doSignIn(user,password) {
    this.service.doSignIn(user, password).subscribe(
      response => {
        // console.dir(response);
        this.processSignInResponse(response);

      },
      error => {
        this.errorInSignup(error)
      },
      () => {
        console.log('SignIn Complete')
      });


  }

  processSignInResponse(response) {
    console.dir(response);
    let responseUser:Users = null;
    if (!response['success']) {
    } else {

      responseUser = <Users>response.data;
      localStorage.setItem('user_object',JSON.stringify(responseUser));
      if(responseUser.role_id == 0){
        //user is admin
        this.router.navigate(['adminpage']);
      }else if(responseUser.role_id == 1){
        this.router.navigate(['userpage']);
      }
    }
  }


}
