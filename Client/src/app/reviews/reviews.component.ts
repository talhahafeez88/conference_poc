import {Component, OnInit, NgZone, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import {UsermenuComponent} from "../usermenu/usermenu.component";
import {RouterLink, ROUTER_DIRECTIVES, Router, ActivatedRoute} from "@angular/router";
import {Review} from '../models/index';
import {PocServiceService} from "../poc-service.service";
import {Document,ConferenceDocument,Conference} from '../models/index';
@Component({
  moduleId: module.id,
  selector: 'app-reviews',
  templateUrl: 'reviews.component.html',
  styleUrls: ['reviews.component.css'],
  directives:[UsermenuComponent,ROUTER_DIRECTIVES]
})



export class ReviewsComponent implements OnInit,AfterViewInit {
  @ViewChild('closeModal')

    closeModal:ElementRef;
  items:Array<any>;
  searchResults:Array<any>;
  currentSelection:string;
  private sub:any;
  current_status:number;

  conference: Conference;
  commentConf: Document;
  conference_id:number;
  isActive:boolean = false;
  isOpen:boolean = false;
  isUser:boolean = false;
  show_submit:boolean = false;
  uploadFile: any;
  uploadProgress: number;
  uploadResponse: Object;
  zone: NgZone;
  genURL:string;
  document_submitted;
  conference_documents:any;

  filesToUpload: Array<File>;
  reviewToUpload:Array<File>;

  document_review:any;


  constructor(private router: Router,private route:ActivatedRoute,private service:PocServiceService) {

    this.items = [];
    this.searchResults = [];
    this.currentSelection= "Open Reviews";
    this.commentConf = new Document();
  }

  ngAfterViewInit() {
    // sketchElement is usable
  }



  onRowClick(user){
    var conf = this.searchResults[user.rowIndex - 1];
    console.dir(conf);

    if(this.current_status == 1){
      //Open
   //   this.router.navigate(['conferencedetails',conf['id'],1,0,1]);
    }else if(this.current_status == 2){
      //Active
   //   this.router.navigate(['conferencedetails',conf['id'],0,1,1]);
    }else if(this.current_status == 3){
      //History
    //  this.router.navigate(['conferencedetails',conf['id'],0,0,1]);
    }


//    this.router.navigate(['conferencedetails',conf['id']]);
  }


  getAllReviews(state){
    this.service.getAllReviews(state,this.service.getCurrentUser().id).subscribe(
      response => {
        // console.dir(response);
        this.processResponse(response);

      },
      error => {
        this.logError(error)
      },
      () => {
        console.log('Get Reviews All Complete')
      });
  }

  processResponse(response){


    console.dir(response);

    if(response.hasOwnProperty(['success'])){
      if(response['data'].length > 0){

        var conferences = response['data'];
        var index = 0;
        console.dir(conferences);
        for(; index < conferences.length ; index++){
          var conf:Review = <Review> conferences[index];
          console.dir(conf);
          this.items.push(conf);

        }

      }
      this.searchResults = this.items;
    }
  }


  logError(error){
    console.error("Error in getAllConference() " + error);
  }

  ngOnInit() {
    //console.log(this.route.params['type']);

    this.sub = this.route.params.subscribe(params => {
      let type = params['type']; // (+) converts string 'id' to a number

      this.updateSelectionForConference(type);
    });
  }

  updateSelectionForConference(value:string){

   if(value === 'open'){
      this.currentSelection = "Open Reviews";
      this.current_status = 1;
      this.items = [];
      this.searchResults = [];
      this.getAllReviews(1);
    }else if(value == 'closed'){
      this.currentSelection = "Closed reviews";
      this.current_status = 3;
      this.items = [];
      this.searchResults = [];
      this.getAllReviews(0);
    }
  }

  getStatusForConference(value){

    if(value == 0){
      return 'Closed';
    }else if(value == 1){
      return 'Active';
    }else if(value == 2){
      return 'Open';
    }

  }



  getRoleString(value){
    if(value == '1'){
      return "Reviewer";
    }else if(value == '2'){
      return 'Author';
    }
  }



  /**
   * Review Upload methods
   */

  onReviewUpload(document:Review){
    alert('review upload ' + document.id);
    this.document_review = document;
    this.commentConf = null;
    this.commentConf = new Document();
  }

  reviewUpload() {

    if(!this.reviewToUpload){
      alert('there is no file');
      if(!this.commentConf.comments){
      }else{
      }

    }else{

      this.reviewMakeFileRequest("http://localhost:3000/upload", [], this.reviewToUpload).then((result) => {
        console.log(result);
      }, (error) => {
        console.error(error);
      });


      if(!this.commentConf.comments){
      }else{
      }

    }



  }

  reviewFileChangeEvent(fileInput: any){
    this.reviewToUpload = <Array<File>> fileInput.target.files;

  }

  reviewMakeFileRequest(url: string, params: Array<string>, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      var me = this;
      for(var i = 0; i < files.length; i++) {
        formData.append("upload", files[i], files[i].name);
      }
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            console.log('respons eis ' + xhr.response);

            var response = JSON.parse(xhr.response);
            if(response['success']){
              alert('file submitted successfully');
              var origname = response['data']['originalname'];
              var path = response['data']['path'];
              alert(path);
                 me.updateDocumentOnServer(origname,path);

            }
            // resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }


  updateDocumentOnServer(filename:any,filepath:any) {
    var json = {
      "review_id": this.document_review.id,
      "doc_name": filename,
      "doc_path": filepath,
      "comments": this.commentConf.comments
    };


    this.service.submitReview(json).subscribe(
      response => {
        console.dir(response);
        this.closeModal.nativeElement.click();
        console.log(this.closeModal);
      },
      error => {
        this.logError(error);
      },
      () => {
        console.log('Document upload Complete');
      });

    console.dir(JSON.stringify(json));
  }






  }
