import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import {Users} from './models/index';
import {PocServiceService} from "./poc-service.service";

@Injectable()
export class AuthorizationService {

  private loggedIn = false;
  login_error = false;

  constructor(private http:Http,private service:PocServiceService) {
    this.loggedIn = !!localStorage.getItem('is_logged_in');
  }

  login(email,password){

    this.service.doSignIn(email, password).subscribe(
      response => {
        // console.dir(response);
        this.processSignInResponse(response);

      },
      error => {
        this.errorInSignup(error)
      },
      () => {
        console.log('SignIn Complete')
      });

  }


  processSignInResponse(response) {
    console.dir(response);
    let responseUser:Users = null;
    if (!response['success']) {
      this.login_error = false;
    } else {

      responseUser = <Users>response.data;
      if(responseUser.role_id == 0){
      }else if(responseUser.role_id == 1){

      }
      this.login_error = true;
    }
  }



  errorInSignup(response) {
    alert('An Error occured during Signin. Please try again later');
  }
}


