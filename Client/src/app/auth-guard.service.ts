import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {Users} from './models/index';
@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private router:Router) {
  }

  canActivate() {
    var rootUser = localStorage.getItem('user_object');
    if(rootUser){
    var user:Users = <Users>JSON.parse(rootUser);

    if (user.id > 0) {
      return true;
    }
    }


    this.router.navigate(['landing']);
    return false;

  }

}
