import {Component, OnInit} from '@angular/core';
import {Config} from './config.service';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {LandingComponent} from "./landing/landing.component";
import {FooterComponent} from "./footer/footer.component";
import {SignupComponent} from "./signup/signup.component";
import {LoggedinComponent} from "./loggedin/loggedin.component";
import {PocServiceService} from "./poc-service.service";


@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  directives: [LoginComponent, LandingComponent, FooterComponent, SignupComponent, LoggedinComponent, ROUTER_DIRECTIVES]
})
export class AppComponent implements OnInit {
  title = Config.APPLICATION_NAME;

  router:Router;

  constructor(router:Router, private service:PocServiceService) {
    this.router = router;
  }

  ngOnInit() {


    var user = this.service.getCurrentUser();

    if(!user){
      this.router.navigate(['landing']);

    }else {
      if (user.id > 0) {


        if (user.role_id == 0) {
          this.router.navigate(['adminpage']);
        } else {
          this.router.navigate(['userpage']);
        }

      } else {
        this.router.navigate(['landing']);
      }
    }
  }
}
