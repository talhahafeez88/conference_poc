import { Component, OnInit } from '@angular/core';
import {RouterLink, ROUTER_DIRECTIVES, Router} from "@angular/router";
import {LoggedinComponent} from "../loggedin/loggedin.component";
import {Conference,Response} from "../models/index";
import {PocServiceService} from "../poc-service.service";


declare var jQuery:any;
@Component({
  moduleId: module.id,
  selector: 'app-addconference',
  templateUrl: 'addconference.component.html',
  styleUrls: ['addconference.component.css'],
  directives:[RouterLink,ROUTER_DIRECTIVES,LoggedinComponent],
  providers:[PocServiceService]
})

export class AddconferenceComponent implements OnInit {

  router:Router;
  conference:Conference;
  statuses=['Active','Open'];
  form_error=true;
  constructor(router:Router,private service:PocServiceService) {
    this.router = router;
    this.conference = new Conference();
    this.conference.status = 1;
  }

  ngOnInit() {

  }

  createConference(){
    this.form_error = false;
      this.service.createNewConference(this.conference).subscribe(
      response => {
        console.dir(response);
        this.processResponse(response);

      },
      error => {
        this.logError(error)
      },
      () => {
        console.log('SignIn Complete')
      });

  }


  processResponse(respone){
      var confResponse:Response = <Response>respone;

      if(confResponse.success){
        this.form_error = true;
//          alert('Conference created Successfully');

        this.router.navigate(['conferences']);
      }else{
          this.form_error = false;
      }
  }

  logError(error){

  }

  statusChanged(value){
    if(value == 'Active'){
      this.conference.status = 1;
    }else{
      this.conference.status = 2;
    }
  }
}
