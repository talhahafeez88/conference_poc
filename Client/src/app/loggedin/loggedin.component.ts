///<reference path="../../../typings/globals/jquery/index.d.ts" />

import { Component, ElementRef, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, Router} from "@angular/router";

declare var jQuery:any;

@Component({
  moduleId: module.id,
  selector: 'app-loggedin',
  templateUrl: 'loggedin.component.html',
  styleUrls: ['loggedin.component.css'],
  directives:[ROUTER_DIRECTIVES]
})
export class LoggedinComponent implements OnInit {

  elementRef: ElementRef;
  items:Array<string>;
  searchResults:Array<string>;
  router:Router;
  constructor(router:Router) {

    this.items = ['1','2','3','4'];
    this.searchResults = this.items;
    this.router = router;
  }



  ngOnInit(){

    console.log('onInit');
    var me = true;



     // jQuery(this.elementRef.nativeElement.contains('#grid-basic')).bootgrid();
  }

  onKeyUp(value:string){
    console.log('key up' + value);

    var results = [];
    for (var i = 0; i < this.items.length; i++) {
      if (this.items[i].indexOf(value) == 0) {
        results.push(this.items[i]);
      }
    }
    this.searchResults = results;




  }

  onRowClick(){
    alert('row clicked');
  }


  doLogout(){
    localStorage.setItem('user_object','');
    this.router.navigate(['landing']);
  }
}
