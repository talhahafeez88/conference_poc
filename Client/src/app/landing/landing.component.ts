import { Component, OnInit } from '@angular/core';
import {LoginComponent} from "../login/login.component";
import {UsermenuComponent} from "../usermenu/usermenu.component";

@Component({
  moduleId: module.id,
  selector: 'app-landing',
  templateUrl: 'landing.component.html',
  styleUrls: ['landing.component.css'],
  directives:[LoginComponent,UsermenuComponent]
})
export class LandingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
