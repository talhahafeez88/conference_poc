import { Component, OnInit } from '@angular/core';
import {LoggedinComponent} from "../loggedin/loggedin.component";
import {UsersComponent} from "../users/users.component";
import {ConferencesComponent} from "../conferences/conferences.component";
import {ConferencedetailsComponent} from "../conferencedetails/conferencedetails.component";
import {AddconferenceComponent} from "../addconference/addconference.component";
import {RouterLink, ROUTER_DIRECTIVES, Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-admin',
  templateUrl: 'admin.component.html',
  styleUrls: ['admin.component.css'],
  directives:[LoggedinComponent,UsersComponent,ConferencesComponent,ConferencedetailsComponent,AddconferenceComponent,
    RouterLink,ROUTER_DIRECTIVES
              ]
})
export class AdminComponent implements OnInit {

  router:Router;
  constructor(router:Router) {
    this.router = router;
  }

  ngOnInit() {

  }

}
