import {Component, OnInit} from '@angular/core';
import {Config} from '../config.service';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import {Users} from '../models/index';
import {PocServiceService} from "../poc-service.service";

@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css'],
  directives: [ROUTER_DIRECTIVES],
  providers: [PocServiceService]
})
export class LoginComponent implements OnInit {

  title:string;
  router:Router;
  user:Users;
  service:PocServiceService;
  login_error = true;

  constructor(router:Router, service:PocServiceService) {
    this.title = Config.APPLICATION_NAME;
    this.router = router;
    this.service = service;
    this.login_error = true;
    this.user = new Users("", "");
  }

  ngOnInit() {

  }


  doSignIn(f) {
    this.login_error = false;
    this.service.doSignIn(this.user.email, this.user.password).subscribe(
      response => {
        // console.dir(response);
        this.processSignInResponse(response);

      },
      error => {
        this.errorInSignup(error)
      },
      () => {
        console.log('SignIn Complete')
      });


  }

  processSignInResponse(response) {
    console.dir(response);
    let responseUser:Users = null;
    if (!response['success']) {
      this.login_error = false;
    } else {

      responseUser = <Users>response.data;
      localStorage.setItem('user_object',JSON.stringify(responseUser));
      if(responseUser.role_id == 0){
        //user is admin
        this.router.navigate(['adminpage']);
      }else if(responseUser.role_id == 1){
        this.router.navigate(['userpage']);
      }
      this.login_error = true;
    }
  }


  doSignup(){
    this.router.navigate(['signup']);
  }

  doLogout() {
    this.router.navigate(['defaultroute'])
  }


  errorInSignup(response) {
    alert('An Error occured during Signin. Please try again later');
  }
}
