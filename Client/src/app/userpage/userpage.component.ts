import { Component, OnInit } from '@angular/core';
import {UsermenuComponent} from "../usermenu/usermenu.component";
import {RouterLink, ROUTER_DIRECTIVES, Router, ActivatedRoute} from "@angular/router";
import {Conference} from '../models/index';
import {PocServiceService} from "../poc-service.service";
@Component({
  moduleId: module.id,
  selector: 'app-userpage',
  templateUrl: 'userpage.component.html',
  styleUrls: ['userpage.component.css'],
  directives:[UsermenuComponent,ROUTER_DIRECTIVES]
})
export class UserpageComponent implements OnInit {


  items:Array<string>;
  searchResults:Array<string>;
  currentSelection:string;
  private sub:any;
  current_status:number;
  constructor(private router: Router,private route:ActivatedRoute,private service:PocServiceService) {

    this.items = [];
    this.searchResults = [];
    this.currentSelection= "Open Conferences to Apply ";


  }



  onRowClick(user){
    var conf = this.searchResults[user.rowIndex - 1];
    console.dir(conf);

    if(this.current_status == 1){
      //Open
      this.router.navigate(['conferencedetails',conf['id'],1,0,1]);
    }else if(this.current_status == 2){
      //Active
      this.router.navigate(['conferencedetails',conf['id'],0,1,1]);
    }else if(this.current_status == 3){
      //History
      this.router.navigate(['conferencedetails',conf['id'],0,0,1]);
    }


//    this.router.navigate(['conferencedetails',conf['id']]);
  }


  getAllConferences(){
    this.service.getAllConferences().subscribe(
      response => {
        // console.dir(response);
        this.processResponse(response);

      },
      error => {
        this.logError(error)
      },
      () => {
        console.log('Get All Conferences Complete')
      });
  }

  processResponse(response){


     console.dir(response);

      if(response.hasOwnProperty(['success'])){
          if(response['data'].length > 0){

             var conferences = response['data'];
              var index = 0;
            for(; index < conferences.length ; index++){
              var conf = conferences[index];
              console.dir(conf);
                this.items.push(conf);

            }

          }
          this.searchResults = this.items;
      }
  }


  logError(error){
    console.error("Error in getAllConference() " + error);
  }

  ngOnInit() {
    //console.log(this.route.params['type']);

    this.sub = this.route.params.subscribe(params => {
      let type = params['type']; // (+) converts string 'id' to a number

      this.updateSelectionForConference(type);
      });
  }

  updateSelectionForConference(value:string){

    if(value === 'active'){
      this.currentSelection = "Active Conferences to Apply";
      this.current_status = 2;
      this.items = [];
      this.searchResults = [];
      this.getMyConferences(1);

    }else if(value === 'open'){
      this.currentSelection = "Open Conferences to Apply";
      this.current_status = 1;
      this.items = [];
      this.searchResults = [];
      this.getAllConferences();
    }else if(value == 'history'){
      this.currentSelection = "Conference History";
      this.current_status = 3;
      this.items = [];
      this.searchResults = [];
      this.getMyConferences(0);
    }
  }

  getStatusForConference(value){

    if(value == 0){
        return 'Closed';
    }else if(value == 1){
        return 'Active';
    }else if(value == 2){
      return 'Open';
    }

  }


  getMyConferences(state){
    this.service.getMyConferences(this.service.getCurrentUser().id,state).subscribe(
      response => {
        // console.dir(response);
        this.processResponse(response);

      },
      error => {
        this.logError(error)
      },
      () => {
        console.log('Get All Conferences Complete')
      });
  }

}
