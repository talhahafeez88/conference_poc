export class Users {

  public id:number;
  public first_name:string;
  public last_name:string;
  public age:number;
  public gender:number;
  public email:string;
  public password:string;
  public role_id:number;
  public status:number;

  constructor(u_email:string,u_pass:string){

    this.email = u_email;
    this.password = u_pass;
  }

}
