export class Document{

  path:string;
  documentName:string;
  description:string;
  conferenceID:number;
  userID:number;
  roleID:number;
  comments:string;

  constructor(){

  }

}
