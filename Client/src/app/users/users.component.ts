import { Component, OnInit } from '@angular/core';
import {ROUTER_DIRECTIVES} from "@angular/router";
import {Response,Users} from '../models/index';
import {PocServiceService} from "../poc-service.service";
@Component({
  moduleId: module.id,
  selector: 'app-users',
  templateUrl: 'users.component.html',
  styleUrls: ['users.component.css'],
  directives:[ROUTER_DIRECTIVES]

})
export class UsersComponent implements OnInit {


  items:Array<Users>;
  searchResults:Array<Users>;
  gender:Array<string>;
  roles:Array<string>;
  constructor(private service:PocServiceService) {

    this.items = [];
    this.searchResults = this.items;
    this.gender = ['Male','Female',"Other"];
    this.roles = ['Reviewer','Author'];

  }

  ngOnInit() {

    console.log('onInit');
    this.getAllUsers();
  }


  onKeyUp(value:string){
    console.log('key up' + value);

    var results = [];
    for (var i = 0; i < this.items.length; i++) {
      if (this.items[i].email.indexOf(value) == 0) {
        results.push(this.items[i]);
      }
    }
    this.searchResults = results;
  }

  onRowClick(){
  //  alert('row clicked');
  }



  getAllUsers(){
    this.service.getAllUsers().subscribe(
      response => {
        // console.dir(response);
        this.processResponse(response);

      },
      error => {
        this.logError(error)
      },
      () => {
        console.log('Get All Conferences Complete')
      });
  }

  processResponse(response){


    var confResponse:Response = <Response>response;


    if(confResponse.success){
      var index = 0;

      for(;index < confResponse.data.length;index++){
        var conf = confResponse.data[index];
        console.dir(conf);
        this.items.push(conf);
      }
      this.searchResults = this.items;

    }else{
      this.searchResults = [];
    }


  }


  logError(error){
    console.error("Error in getAllConference() " + error);
  }


  getGenderString(value){
      if(value == '1'){
        return "Male";
      }else if(value == '2'){
        return 'Female';
      }else{
        return 'Other';
      }

  }

  roleChanged(value,user_id){
    var role_id = -1;

    if(value == 'Reviewer'){
        role_id = 1;
    }else{
      role_id = 2;
    }

   this.updateUserStatus(user_id,role_id);
    console.log('role changed' + value);
  }

  getRoleString(value){
    if(value == '1'){
      return "Reviewer";
    }else if(value == '2'){
      return 'Author';
    }

  }

  updateRole(user_id){
    var updatedUser = this.getUserWithUserId(user_id);
    console.dir(updatedUser);


    this.service.updateUser(updatedUser.id,updatedUser.status).subscribe(
      response => {
        console.dir(response);

      },
      error => {
        this.logError(error)
      },
      () => {
        console.log('Get All Conferences Complete')
      });

  }


  updateUserStatus(user_id,role_id){
    for(var index = 0 ; index < this.searchResults.length ;index++){
        var user:Users = <Users>this.searchResults[index];

      if(user.id == user_id ){
          user.status = role_id;
        break;
      }
    }
  }



  getUserWithUserId(user_id){
    for(var index = 0 ; index < this.searchResults.length ;index++){
      var user:Users = <Users>this.searchResults[index];

      if(user.id == user_id ){
          return user;
        }
    }
  }
}
