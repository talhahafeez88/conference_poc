import { Component, OnInit,NgZone } from '@angular/core';
import {LoggedinComponent} from "../loggedin/loggedin.component";
import {ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {Conference,Response} from "../models/index";
import {PocServiceService} from "../poc-service.service"
import {UsermenuComponent} from "../usermenu/usermenu.component";
import {UPLOAD_DIRECTIVES} from 'ng2-uploader/ng2-uploader';
import {Document,ConferenceDocument} from '../models/index';

@Component({
  moduleId: module.id,
  selector: 'app-conferencedetails',
  templateUrl: 'conferencedetails.component.html',
  styleUrls: ['conferencedetails.component.css'],
  directives:[LoggedinComponent,ROUTER_DIRECTIVES,UsermenuComponent,UPLOAD_DIRECTIVES],
  providers:[PocServiceService]
})
export class ConferencedetailsComponent implements OnInit {


  private sub:any;
  conference: Conference;
  commentConf: Document;
  conference_id:number;
  isActive:boolean = false;
  isOpen:boolean = false;
  isUser:boolean = false;
  show_submit:boolean = false;
  uploadFile: any;
  uploadProgress: number;
  uploadResponse: Object;
  zone: NgZone;
  genURL:string;
  document_submitted;
  conference_documents:any;

  filesToUpload: Array<File>;
  reviewToUpload:Array<File>;

  document_review:any;

  options: Object = {
    url: 'http://localhost/fs/upload.php'
  };


  constructor(private router: Router,private route:ActivatedRoute,private service:PocServiceService) {

    this.uploadProgress = 0;
    this.uploadResponse = {};
    this.zone = new NgZone({ enableLongStackTrace: false });
    this.filesToUpload = [];


  }

  ngOnInit() {


    this.conference = new Conference();
    this.commentConf = new Document();
    this.commentConf = null;
    this.commentConf = new Document();
    this.filesToUpload = [];
    this.sub = this.route.params.subscribe(params => {
     this.conference_id = params['conference']; // (+) converts string 'id' to a number

      if(params['isActive'] == 0){
          this.isActive = false;
          console.log('isactive is false');
      }else if(params['isActive'] == 1){
        this.isActive = true;
        console.log('isactive is true');
      }

      if(params['isOpen'] == 0){
        this.isOpen = false;
        console.log('isOpen is false');
      }else if(params['isOpen'] == 1){
        this.isOpen = true;
        console.log('isOpen is true');
      }



      if(params['isUser'] == 0){
        this.isUser = false;
        console.log('isUser is false');
      }else if(params['isUser'] == 1){
        this.isUser = true;
        console.log('isUser is true');
      }

      this.getConferenceDetails(this.conference_id);
      this.getDocumentsForConference();
    });

  }

  getConferenceDetails(conf_id){
    this.service.getConferenceDetails(conf_id).subscribe(
      response => {
        console.dir(response);
        this.processResponse(response);

      },
      error => {
        this.logError(error)
      },
      () => {
        console.log('Conference Details Complete')
      });

  }


  processResponse(respone){
    var confResponse:Response = <Response>respone;

    if(confResponse.success){
     //  alert('Conference created Successfully');

        console.dir(confResponse.data[0]);
        this.conference = <Conference> confResponse.data[0];

    }else{
    }
  }

  logError(error){

  }


  getStatusForConference(value){

    if(value == 0){
      return 'Closed';
    }else if(value == 1){
      return 'Active';
    }else if(value == 2){
      return 'Open';
    }

  }


  joinConference(){

    this.service.joinConference(this.conference_id,this.service.getCurrentUser().id).subscribe(
      response => {
        console.dir(response);
        this.processJoinResponse(response);

      },
      error => {
        this.logError(error);
      },
      () => {
        console.log('Conference Details Complete');
      });

  }



  processJoinResponse(respone){
    var confResponse:Response = <Response>respone;

    if(confResponse.success){

         console.dir(confResponse.data[0]);

        this.router.navigate(['conferencedetails',this.conference_id,0,1,1]);


    }else{
    }
  }




  submitDocoument(){
    this.show_submit = true;
    this.document_submitted = false;
  }


  updateDocumentOnServer(fileName:string,fileURL:string){

  //   console.dir(this.uploadResponse['generatedName']);
      this.genURL = fileURL;

    var doc:Document = new Document();
    doc.documentName = fileName;
    doc.description = "";
    doc.conferenceID = this.conference_id;
    doc.userID = this.service.getCurrentUser().id;
    doc.path = this.genURL;
    doc.roleID = this.service.getCurrentUser().role_id;
    doc.comments = this.commentConf.comments;
    this.uploadFile = doc;
    this.service.submitDocument(doc).subscribe(
      response => {
        console.dir(response);
        this.document_submitted = true;
        this.show_submit = false;

        this.getDocumentsForConference();

      },
      error => {
        this.logError(error);
      },
      () => {
        console.log('Document upload Complete');
      });


  }


  getDocumentsForConference(){
    this.service.getDocumentsForConference(this.conference_id).subscribe(
      response => {
        console.dir(response);
        this.conference_documents = [];

        this.document_submitted = false;
        this.show_submit = false;

        if(response['data'] != null){
            var responseDoc = response['data'];
              var index = 0 ;
          for(;index < responseDoc.length; index++){
              var conf:ConferenceDocument = <ConferenceDocument>responseDoc[index];
              this.conference_documents.push(conf);
          }

          console.dir(this.conference_documents);
        }else{

        }

      },
      error => {
        this.logError(error);
      },
      () => {
        console.log('Document details Complete');
      });
  }


    /**
     * File Upload methods
     */

    upload() {
      this.makeFileRequest("http://localhost:3000/upload", [], this.filesToUpload).then((result) => {
        console.log(result);
      }, (error) => {
        console.error(error);
      });
    }

  fileChangeEvent(fileInput: any){
    this.filesToUpload = <Array<File>> fileInput.target.files;
  }

  makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      var me = this;
      for(var i = 0; i < files.length; i++) {
        formData.append("upload", files[i], files[i].name);
      }
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            console.log('respons eis ' + xhr.response);

            var response = JSON.parse(xhr.response);
            if(response['success']){
              alert('file submitted successfully');
              var origname = response['data']['originalname'];
              var path = response['data']['path'];
              me.updateDocumentOnServer(origname,path);

            }
            // resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }


  onReviewUpload(document:Document){
    alert('review upload ' + document.documentName);
    this.document_review = new Document();

  }



  /**
   * Review Upload methods
   */

  reviewUpload() {

    if(!this.reviewToUpload){
      alert('there is no file');
      if(!this.commentConf.comments){
      }else{
      }

    }else{

      this.reviewMakeFileRequest("http://localhost:3000/upload", [], this.reviewToUpload).then((result) => {
        console.log(result);
      }, (error) => {
        console.error(error);
      });


      if(!this.commentConf.comments){
      }else{
      }

    }



  }

  reviewFileChangeEvent(fileInput: any){
    this.reviewToUpload = <Array<File>> fileInput.target.files;

  }

  reviewMakeFileRequest(url: string, params: Array<string>, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      var me = this;
      for(var i = 0; i < files.length; i++) {
        formData.append("upload", files[i], files[i].name);
      }
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            console.log('respons eis ' + xhr.response);

            var response = JSON.parse(xhr.response);
            if(response['success']){
              alert('file submitted successfully');
              var origname = response['data']['originalname'];
              var path = response['data']['path'];
              alert(path);
           //   me.updateDocumentOnServer(origname,path);

            }
            // resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

  getRoleString(value){
    if(value == '1'){
      return "Reviewer";
    }else if(value == '2'){
      return 'Author';
    }

  }

}
