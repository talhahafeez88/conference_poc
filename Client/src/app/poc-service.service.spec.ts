/* tslint:disable:no-unused-variable */

import { addProviders, async, inject } from '@angular/core/testing';
import { PocServiceService } from './poc-service.service';

describe('Service: PocService', () => {
  beforeEach(() => {
    addProviders([PocServiceService]);
  });

  it('should ...',
    inject([PocServiceService],
      (service: PocServiceService) => {
        expect(service).toBeTruthy();
      }));
});
