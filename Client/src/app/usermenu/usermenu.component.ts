import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, Router} from "@angular/router";
@Component({
  moduleId: module.id,
  selector: 'app-usermenu',
  templateUrl: 'usermenu.component.html',
  styleUrls: ['usermenu.component.css'],
  directives:[ROUTER_DIRECTIVES]
})
export class UsermenuComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }


  doLogout(){
    localStorage.setItem('user_object','');
    this.router.navigate(['landing']);
  }

}
