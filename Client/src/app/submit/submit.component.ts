import { Component, OnInit } from '@angular/core';
import {UPLOAD_DIRECTIVES} from 'ng2-uploader/ng2-uploader';

@Component({
  moduleId: module.id,
  selector: 'app-submit',
  templateUrl: 'submit.component.html',
  styleUrls: ['submit.component.css'],
  directives: [UPLOAD_DIRECTIVES],
})
export class SubmitComponent implements OnInit {


  uploadedFiles: any[] = [];
  options: Object = {
    url: 'http://localhost/fs/upload.php'
  };

  handleUpload(data): void {
    if (data && data.response) {
      data = JSON.parse(data.response);
      this.uploadedFiles.push(data);
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
