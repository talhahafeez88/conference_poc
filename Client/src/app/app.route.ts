/**
 * Created by apple on 25/07/2016.
 */
import { provideRouter, RouterConfig } from '@angular/router';
import {SignupComponent} from "./signup/signup.component";
import {LandingComponent} from "./landing/landing.component";
import {LoggedinComponent} from "./loggedin/loggedin.component";
import {ConferencesComponent} from "./conferences/conferences.component";
import {UsersComponent} from "./users/users.component";
import {AddconferenceComponent} from "./addconference/addconference.component";
import {ConferencedetailsComponent} from "./conferencedetails/conferencedetails.component";
import {AdminComponent} from "./admin/admin.component";
import {AppComponent} from "./app.component";
import {UserpageComponent} from "./userpage/userpage.component";
import {SubmitComponent} from "./submit/submit.component";
import {AuthGuardService} from "./auth-guard.service";
import {ReviewsComponent} from "./reviews/reviews.component";

const routes: RouterConfig = [
    {
        path:'',
        component:AppComponent,
    },
    {path:'signup',component:SignupComponent},
    {path:'landing',component:LandingComponent},
    {path:'loggedin',component:LoggedinComponent},
    {path:'conferences',component:ConferencesComponent},
    {path:'users',component:UsersComponent},
    {path:'addconference',component:AddconferenceComponent},
    {path:'conferencedetails/:conference',component:ConferencedetailsComponent},
    {path:'adminpage',component:AdminComponent},
    {path:'userpage',component:UserpageComponent, canActivate:[AuthGuardService]},
    {path:'submit/:conf_id',component:SubmitComponent},
    {path:'userpage/:type',component:UserpageComponent},
    {
      path:'defaultroute',
      component:AppComponent
    },
    {path:'conferencedetails/:conference/:isActive/:isOpen/:isUser',component:ConferencedetailsComponent,canActivate:[AuthGuardService]},
    {path:'reviews/:type',component:ReviewsComponent},
];

export const appRouterProviders = [
    provideRouter(routes)
];
