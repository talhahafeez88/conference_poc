import { Injectable } from '@angular/core';
import {Http,HTTP_PROVIDERS,Headers} from '@angular/http';
import {Users,Document} from './models/index';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class PocServiceService {

  private _http:Http;
  private _baseURL:string;
  private loggedIn:boolean;

  constructor(http:Http) {
    this._baseURL="http://localhost:3000";
    this._http = http;
    let user = localStorage.getItem('user_object');
    if(user != null){
      this.loggedIn = true;
    }else{
      this.loggedIn = false;
    }
  }

  public doSignIn(email:string,password:string){

      console.log('signin request result is ' + email + password);

     return this.processSignIn(email,password);


  }

  public doSignup(value){
     return this.processSignup(value);

  }


  processSignIn(email:string,password:string){

    console.log('Web Service Call');
    var username = email;
    var password = password;
    var url = this._baseURL + '/signin'

    var creds = "email=" + username + "&password=" + password;


    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

     return this._http.post(url, creds, {
      headers: headers
    })
      .map(res => res.json());


  }



  processSignup(signupForm){

    console.log('Web Service Call');
    var url = this._baseURL + '/register';

    console.log(url);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    console.log(signupForm);
    return this._http.post(
          url,
          signupForm,
          {
              headers: headers
          }
    )
     .map(
       response => response.json()
     );


  }



  getAllConferences(){
    console.log('Web Service Call');
    var url = this._baseURL + '/getAllConferences';

    console.log(url);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var user_id = this.getCurrentUser().id;
    return this._http.post(
      url,
      {"user_id":user_id},
      {
        headers: headers
      }
    )
      .map(
        response => response.json()
      );
  }






  createNewConference(value){
    console.log('Web Service Call ' + JSON.stringify(value));
    var url = this._baseURL + '/createConference';

    console.log(url);
    var headers = new Headers();
    var conference = JSON.stringify(value);
    headers.append('Content-Type', 'application/json');

    return this._http.post(
      url,
      conference,
      {
        headers: headers
      }
    )
      .map(
        response => response.json()
      );
  }


  //getAllConferencesAdmin

  getAllConferencesAdmin(){
    console.log('Web Service Call');
    var url = this._baseURL + '/getAllConferencesAdmin';

    console.log(url);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var user_id = this.getCurrentUser().id;
    return this._http.post(
      url,
      {"user_id":user_id},
      {
        headers: headers
      }
    )
      .map(
        response => response.json()
      );

  }

  getConferenceDetails(conf_id){
    ///getConferenceDetails

    console.log('Web Service Call');
    var url = this._baseURL + '/getConferenceDetails';

    console.log(url);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post(
      url,
      {"conf_id":conf_id},
      {
        headers: headers
      }
    )
      .map(
        response => response.json()
      );
  }


  joinConference(conf_id,user_id){

    console.log('Web Service Call');
    var url = this._baseURL + '/joinConference';

    console.log(url);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post(
      url,
      {
        "conference_id":conf_id,
        "user_id":user_id
      },

      {
        headers: headers
      }
    )
      .map(
        response => response.json()
      );

  }



  getMyConferences(user_id,state){
    console.log('Web Service Call');
    var url = this._baseURL + '/getMyConference';

    console.log(url);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var user_ids = this.getCurrentUser().id;
    return this._http.post(
      url,
      {"user_id":user_ids,
        "state":state
      },
      {
        headers: headers
      }
    )
      .map(
        response => response.json()
      );
  }



  submitDocument(document:Document){

    console.log('Web Service Call');
    var url = this._baseURL + '/submitDocument';

    console.log(url);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    //alert();
    console.dir(JSON.stringify(document));
    return this._http.post(
      url,
     document,
      {
        headers: headers
      }
    )
      .map(
        response => response.json()
      );

  }



  getDocumentsForConference(conf_id){
    console.log('Web Service Call');
    var url = this._baseURL + '/getConfrenceDetailsForUser';

    console.log(url);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var user_id = this.getCurrentUser().id;
    return this._http.post(
      url,
      {"conference_id":conf_id,
        "user_id":user_id
      },
      {
        headers: headers
      }
    )
      .map(
        response => response.json()
      );
  }


  getAllUsers(){
    console.log('Web Service Call');
    var url = this._baseURL + '/getAllUsers';

    console.log(url);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post(
      url,
      {
      },
      {
        headers: headers
      }
    )
      .map(
        response => response.json()
      );
  }




  saveJwt(jwt) {
    if(jwt) {
      console.dir('Error occured with request '+jwt);
    }
  }




  getCurrentUser(){
    var rootUser = localStorage.getItem('user_object');
    if(!rootUser){
      return;
    }
    var user:Users = <Users>JSON.parse(rootUser);
    console.dir(user);

    if(user.id > 0){
      return user;
    }

    return null;

  }

  updateUser(user_id,status_id){

    console.log('Web Service Call');
    var url = this._baseURL + '/updateUser';

    console.log(url);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this._http.post(
      url,
      {
        "status_id":status_id,
        "user_id":user_id
      },
      {
        headers: headers
      }
    )
      .map(
        response => response.json()
      );

  }


  getAllReviews(is_active,user_id){
    console.log('Web Service Call');
    var url = this._baseURL + '/getAllReviews';

    console.log(url);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    //var user_id = this.getCurrentUser().id;
    var is_active_string = null;
    if(is_active == 0){
      is_active_string = "false";
    }else{
      is_active_string = "true";
    }

    console.log({
      "is_active":is_active_string,
      "user_id":user_id
    });
    return this._http.post(
      url,
      {
        "is_active":is_active_string,
        "user_id":user_id
      },
      {
        headers: headers
      }
    )
      .map(
        response => response.json()
      );
  }


  submitReview(json:any){

    console.log('Web Service Call');
    var url = this._baseURL + '/submitReview';

    console.log(url);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    //alert();
   // console.dir(JSON.stringify(document));
    return this._http.post(
      url,
      json,
      {
        headers: headers
      }
    )
      .map(
        response => response.json()
      );

  }


}
