import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { AppComponent, environment } from './app/';
import {appRouterProviders} from "./app/app.route";
import {Http,HTTP_PROVIDERS} from '@angular/http';
import {PocServiceService} from './app/poc-service.service';
import {AuthGuardService} from "./app/auth-guard.service";

if (environment.production) {
  enableProdMode();
}

bootstrap(AppComponent,[appRouterProviders,Http,PocServiceService,HTTP_PROVIDERS,AuthGuardService]);
