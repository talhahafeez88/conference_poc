import { ReviewPOCPage } from './app.po';

describe('review-poc App', function() {
  let page: ReviewPOCPage;

  beforeEach(() => {
    page = new ReviewPOCPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
