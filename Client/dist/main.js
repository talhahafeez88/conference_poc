"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var core_1 = require('@angular/core');
var _1 = require('./app/');
var app_route_1 = require("./app/app.route");
var http_1 = require('@angular/http');
var poc_service_service_1 = require('./app/poc-service.service');
var auth_guard_service_1 = require("./app/auth-guard.service");
if (_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.bootstrap(_1.AppComponent, [app_route_1.appRouterProviders, http_1.Http, poc_service_service_1.PocServiceService, http_1.HTTP_PROVIDERS, auth_guard_service_1.AuthGuardService]);
//# sourceMappingURL=tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/main.js.map