"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var usermenu_component_1 = require("../usermenu/usermenu.component");
var router_1 = require("@angular/router");
var poc_service_service_1 = require("../poc-service.service");
var index_1 = require('../models/index');
var ReviewsComponent = (function () {
    function ReviewsComponent(router, route, service) {
        this.router = router;
        this.route = route;
        this.service = service;
        this.isActive = false;
        this.isOpen = false;
        this.isUser = false;
        this.show_submit = false;
        this.items = [];
        this.searchResults = [];
        this.currentSelection = "Open Reviews";
        this.commentConf = new index_1.Document();
    }
    ReviewsComponent.prototype.ngAfterViewInit = function () {
        // sketchElement is usable
    };
    ReviewsComponent.prototype.onRowClick = function (user) {
        var conf = this.searchResults[user.rowIndex - 1];
        console.dir(conf);
        if (this.current_status == 1) {
        }
        else if (this.current_status == 2) {
        }
        else if (this.current_status == 3) {
        }
        //    this.router.navigate(['conferencedetails',conf['id']]);
    };
    ReviewsComponent.prototype.getAllReviews = function (state) {
        var _this = this;
        this.service.getAllReviews(state, this.service.getCurrentUser().id).subscribe(function (response) {
            // console.dir(response);
            _this.processResponse(response);
        }, function (error) {
            _this.logError(error);
        }, function () {
            console.log('Get Reviews All Complete');
        });
    };
    ReviewsComponent.prototype.processResponse = function (response) {
        console.dir(response);
        if (response.hasOwnProperty(['success'])) {
            if (response['data'].length > 0) {
                var conferences = response['data'];
                var index = 0;
                console.dir(conferences);
                for (; index < conferences.length; index++) {
                    var conf = conferences[index];
                    console.dir(conf);
                    this.items.push(conf);
                }
            }
            this.searchResults = this.items;
        }
    };
    ReviewsComponent.prototype.logError = function (error) {
        console.error("Error in getAllConference() " + error);
    };
    ReviewsComponent.prototype.ngOnInit = function () {
        //console.log(this.route.params['type']);
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var type = params['type']; // (+) converts string 'id' to a number
            _this.updateSelectionForConference(type);
        });
    };
    ReviewsComponent.prototype.updateSelectionForConference = function (value) {
        if (value === 'open') {
            this.currentSelection = "Open Reviews";
            this.current_status = 1;
            this.items = [];
            this.searchResults = [];
            this.getAllReviews(1);
        }
        else if (value == 'closed') {
            this.currentSelection = "Closed reviews";
            this.current_status = 3;
            this.items = [];
            this.searchResults = [];
            this.getAllReviews(0);
        }
    };
    ReviewsComponent.prototype.getStatusForConference = function (value) {
        if (value == 0) {
            return 'Closed';
        }
        else if (value == 1) {
            return 'Active';
        }
        else if (value == 2) {
            return 'Open';
        }
    };
    ReviewsComponent.prototype.getRoleString = function (value) {
        if (value == '1') {
            return "Reviewer";
        }
        else if (value == '2') {
            return 'Author';
        }
    };
    /**
     * Review Upload methods
     */
    ReviewsComponent.prototype.onReviewUpload = function (document) {
        alert('review upload ' + document.id);
        this.document_review = document;
        this.commentConf = null;
        this.commentConf = new index_1.Document();
    };
    ReviewsComponent.prototype.reviewUpload = function () {
        if (!this.reviewToUpload) {
            alert('there is no file');
            if (!this.commentConf.comments) {
            }
            else {
            }
        }
        else {
            this.reviewMakeFileRequest("http://localhost:3000/upload", [], this.reviewToUpload).then(function (result) {
                console.log(result);
            }, function (error) {
                console.error(error);
            });
            if (!this.commentConf.comments) {
            }
            else {
            }
        }
    };
    ReviewsComponent.prototype.reviewFileChangeEvent = function (fileInput) {
        this.reviewToUpload = fileInput.target.files;
    };
    ReviewsComponent.prototype.reviewMakeFileRequest = function (url, params, files) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            var me = _this;
            for (var i = 0; i < files.length; i++) {
                formData.append("upload", files[i], files[i].name);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        console.log('respons eis ' + xhr.response);
                        var response = JSON.parse(xhr.response);
                        if (response['success']) {
                            alert('file submitted successfully');
                            var origname = response['data']['originalname'];
                            var path = response['data']['path'];
                            alert(path);
                            me.updateDocumentOnServer(origname, path);
                        }
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    };
    ReviewsComponent.prototype.updateDocumentOnServer = function (filename, filepath) {
        var _this = this;
        var json = {
            "review_id": this.document_review.id,
            "doc_name": filename,
            "doc_path": filepath,
            "comments": this.commentConf.comments
        };
        this.service.submitReview(json).subscribe(function (response) {
            console.dir(response);
            _this.closeModal.nativeElement.click();
            console.log(_this.closeModal);
        }, function (error) {
            _this.logError(error);
        }, function () {
            console.log('Document upload Complete');
        });
        console.dir(JSON.stringify(json));
    };
    __decorate([
        core_1.ViewChild('closeModal'), 
        __metadata('design:type', core_1.ElementRef)
    ], ReviewsComponent.prototype, "closeModal", void 0);
    ReviewsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-reviews',
            templateUrl: 'reviews.component.html',
            styleUrls: ['reviews.component.css'],
            directives: [usermenu_component_1.UsermenuComponent, router_1.ROUTER_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, poc_service_service_1.PocServiceService])
    ], ReviewsComponent);
    return ReviewsComponent;
}());
exports.ReviewsComponent = ReviewsComponent;
//# sourceMappingURL=../../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/reviews/reviews.component.js.map