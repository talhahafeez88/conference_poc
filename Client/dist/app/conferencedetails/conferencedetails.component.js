"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var loggedin_component_1 = require("../loggedin/loggedin.component");
var router_1 = require("@angular/router");
var index_1 = require("../models/index");
var poc_service_service_1 = require("../poc-service.service");
var usermenu_component_1 = require("../usermenu/usermenu.component");
var ng2_uploader_1 = require('ng2-uploader/ng2-uploader');
var index_2 = require('../models/index');
var ConferencedetailsComponent = (function () {
    function ConferencedetailsComponent(router, route, service) {
        this.router = router;
        this.route = route;
        this.service = service;
        this.isActive = false;
        this.isOpen = false;
        this.isUser = false;
        this.show_submit = false;
        this.options = {
            url: 'http://localhost/fs/upload.php'
        };
        this.uploadProgress = 0;
        this.uploadResponse = {};
        this.zone = new core_1.NgZone({ enableLongStackTrace: false });
        this.filesToUpload = [];
    }
    ConferencedetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.conference = new index_1.Conference();
        this.commentConf = new index_2.Document();
        this.commentConf = null;
        this.commentConf = new index_2.Document();
        this.filesToUpload = [];
        this.sub = this.route.params.subscribe(function (params) {
            _this.conference_id = params['conference']; // (+) converts string 'id' to a number
            if (params['isActive'] == 0) {
                _this.isActive = false;
                console.log('isactive is false');
            }
            else if (params['isActive'] == 1) {
                _this.isActive = true;
                console.log('isactive is true');
            }
            if (params['isOpen'] == 0) {
                _this.isOpen = false;
                console.log('isOpen is false');
            }
            else if (params['isOpen'] == 1) {
                _this.isOpen = true;
                console.log('isOpen is true');
            }
            if (params['isUser'] == 0) {
                _this.isUser = false;
                console.log('isUser is false');
            }
            else if (params['isUser'] == 1) {
                _this.isUser = true;
                console.log('isUser is true');
            }
            _this.getConferenceDetails(_this.conference_id);
            _this.getDocumentsForConference();
        });
    };
    ConferencedetailsComponent.prototype.getConferenceDetails = function (conf_id) {
        var _this = this;
        this.service.getConferenceDetails(conf_id).subscribe(function (response) {
            console.dir(response);
            _this.processResponse(response);
        }, function (error) {
            _this.logError(error);
        }, function () {
            console.log('Conference Details Complete');
        });
    };
    ConferencedetailsComponent.prototype.processResponse = function (respone) {
        var confResponse = respone;
        if (confResponse.success) {
            //  alert('Conference created Successfully');
            console.dir(confResponse.data[0]);
            this.conference = confResponse.data[0];
        }
        else {
        }
    };
    ConferencedetailsComponent.prototype.logError = function (error) {
    };
    ConferencedetailsComponent.prototype.getStatusForConference = function (value) {
        if (value == 0) {
            return 'Closed';
        }
        else if (value == 1) {
            return 'Active';
        }
        else if (value == 2) {
            return 'Open';
        }
    };
    ConferencedetailsComponent.prototype.joinConference = function () {
        var _this = this;
        this.service.joinConference(this.conference_id, this.service.getCurrentUser().id).subscribe(function (response) {
            console.dir(response);
            _this.processJoinResponse(response);
        }, function (error) {
            _this.logError(error);
        }, function () {
            console.log('Conference Details Complete');
        });
    };
    ConferencedetailsComponent.prototype.processJoinResponse = function (respone) {
        var confResponse = respone;
        if (confResponse.success) {
            console.dir(confResponse.data[0]);
            this.router.navigate(['conferencedetails', this.conference_id, 0, 1, 1]);
        }
        else {
        }
    };
    ConferencedetailsComponent.prototype.submitDocoument = function () {
        this.show_submit = true;
        this.document_submitted = false;
    };
    ConferencedetailsComponent.prototype.updateDocumentOnServer = function (fileName, fileURL) {
        var _this = this;
        //   console.dir(this.uploadResponse['generatedName']);
        this.genURL = fileURL;
        var doc = new index_2.Document();
        doc.documentName = fileName;
        doc.description = "";
        doc.conferenceID = this.conference_id;
        doc.userID = this.service.getCurrentUser().id;
        doc.path = this.genURL;
        doc.roleID = this.service.getCurrentUser().role_id;
        doc.comments = this.commentConf.comments;
        this.uploadFile = doc;
        this.service.submitDocument(doc).subscribe(function (response) {
            console.dir(response);
            _this.document_submitted = true;
            _this.show_submit = false;
            _this.getDocumentsForConference();
        }, function (error) {
            _this.logError(error);
        }, function () {
            console.log('Document upload Complete');
        });
    };
    ConferencedetailsComponent.prototype.getDocumentsForConference = function () {
        var _this = this;
        this.service.getDocumentsForConference(this.conference_id).subscribe(function (response) {
            console.dir(response);
            _this.conference_documents = [];
            _this.document_submitted = false;
            _this.show_submit = false;
            if (response['data'] != null) {
                var responseDoc = response['data'];
                var index = 0;
                for (; index < responseDoc.length; index++) {
                    var conf = responseDoc[index];
                    _this.conference_documents.push(conf);
                }
                console.dir(_this.conference_documents);
            }
            else {
            }
        }, function (error) {
            _this.logError(error);
        }, function () {
            console.log('Document details Complete');
        });
    };
    /**
     * File Upload methods
     */
    ConferencedetailsComponent.prototype.upload = function () {
        this.makeFileRequest("http://localhost:3000/upload", [], this.filesToUpload).then(function (result) {
            console.log(result);
        }, function (error) {
            console.error(error);
        });
    };
    ConferencedetailsComponent.prototype.fileChangeEvent = function (fileInput) {
        this.filesToUpload = fileInput.target.files;
    };
    ConferencedetailsComponent.prototype.makeFileRequest = function (url, params, files) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            var me = _this;
            for (var i = 0; i < files.length; i++) {
                formData.append("upload", files[i], files[i].name);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        console.log('respons eis ' + xhr.response);
                        var response = JSON.parse(xhr.response);
                        if (response['success']) {
                            alert('file submitted successfully');
                            var origname = response['data']['originalname'];
                            var path = response['data']['path'];
                            me.updateDocumentOnServer(origname, path);
                        }
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    };
    ConferencedetailsComponent.prototype.onReviewUpload = function (document) {
        alert('review upload ' + document.documentName);
        this.document_review = new index_2.Document();
    };
    /**
     * Review Upload methods
     */
    ConferencedetailsComponent.prototype.reviewUpload = function () {
        if (!this.reviewToUpload) {
            alert('there is no file');
            if (!this.commentConf.comments) {
            }
            else {
            }
        }
        else {
            this.reviewMakeFileRequest("http://localhost:3000/upload", [], this.reviewToUpload).then(function (result) {
                console.log(result);
            }, function (error) {
                console.error(error);
            });
            if (!this.commentConf.comments) {
            }
            else {
            }
        }
    };
    ConferencedetailsComponent.prototype.reviewFileChangeEvent = function (fileInput) {
        this.reviewToUpload = fileInput.target.files;
    };
    ConferencedetailsComponent.prototype.reviewMakeFileRequest = function (url, params, files) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            var me = _this;
            for (var i = 0; i < files.length; i++) {
                formData.append("upload", files[i], files[i].name);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        console.log('respons eis ' + xhr.response);
                        var response = JSON.parse(xhr.response);
                        if (response['success']) {
                            alert('file submitted successfully');
                            var origname = response['data']['originalname'];
                            var path = response['data']['path'];
                            alert(path);
                        }
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    };
    ConferencedetailsComponent.prototype.getRoleString = function (value) {
        if (value == '1') {
            return "Reviewer";
        }
        else if (value == '2') {
            return 'Author';
        }
    };
    ConferencedetailsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-conferencedetails',
            templateUrl: 'conferencedetails.component.html',
            styleUrls: ['conferencedetails.component.css'],
            directives: [loggedin_component_1.LoggedinComponent, router_1.ROUTER_DIRECTIVES, usermenu_component_1.UsermenuComponent, ng2_uploader_1.UPLOAD_DIRECTIVES],
            providers: [poc_service_service_1.PocServiceService]
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, poc_service_service_1.PocServiceService])
    ], ConferencedetailsComponent);
    return ConferencedetailsComponent;
}());
exports.ConferencedetailsComponent = ConferencedetailsComponent;
//# sourceMappingURL=../../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/conferencedetails/conferencedetails.component.js.map