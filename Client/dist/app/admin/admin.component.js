"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var loggedin_component_1 = require("../loggedin/loggedin.component");
var users_component_1 = require("../users/users.component");
var conferences_component_1 = require("../conferences/conferences.component");
var conferencedetails_component_1 = require("../conferencedetails/conferencedetails.component");
var addconference_component_1 = require("../addconference/addconference.component");
var router_1 = require("@angular/router");
var AdminComponent = (function () {
    function AdminComponent(router) {
        this.router = router;
    }
    AdminComponent.prototype.ngOnInit = function () {
    };
    AdminComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-admin',
            templateUrl: 'admin.component.html',
            styleUrls: ['admin.component.css'],
            directives: [loggedin_component_1.LoggedinComponent, users_component_1.UsersComponent, conferences_component_1.ConferencesComponent, conferencedetails_component_1.ConferencedetailsComponent, addconference_component_1.AddconferenceComponent,
                router_1.RouterLink, router_1.ROUTER_DIRECTIVES
            ]
        }), 
        __metadata('design:paramtypes', [router_1.Router])
    ], AdminComponent);
    return AdminComponent;
}());
exports.AdminComponent = AdminComponent;
//# sourceMappingURL=../../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/admin/admin.component.js.map