"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var usermenu_component_1 = require("../usermenu/usermenu.component");
var router_1 = require("@angular/router");
var poc_service_service_1 = require("../poc-service.service");
var UserpageComponent = (function () {
    function UserpageComponent(router, route, service) {
        this.router = router;
        this.route = route;
        this.service = service;
        this.items = [];
        this.searchResults = [];
        this.currentSelection = "Open Conferences to Apply ";
    }
    UserpageComponent.prototype.onRowClick = function (user) {
        var conf = this.searchResults[user.rowIndex - 1];
        console.dir(conf);
        if (this.current_status == 1) {
            //Open
            this.router.navigate(['conferencedetails', conf['id'], 1, 0, 1]);
        }
        else if (this.current_status == 2) {
            //Active
            this.router.navigate(['conferencedetails', conf['id'], 0, 1, 1]);
        }
        else if (this.current_status == 3) {
            //History
            this.router.navigate(['conferencedetails', conf['id'], 0, 0, 1]);
        }
        //    this.router.navigate(['conferencedetails',conf['id']]);
    };
    UserpageComponent.prototype.getAllConferences = function () {
        var _this = this;
        this.service.getAllConferences().subscribe(function (response) {
            // console.dir(response);
            _this.processResponse(response);
        }, function (error) {
            _this.logError(error);
        }, function () {
            console.log('Get All Conferences Complete');
        });
    };
    UserpageComponent.prototype.processResponse = function (response) {
        console.dir(response);
        if (response.hasOwnProperty(['success'])) {
            if (response['data'].length > 0) {
                var conferences = response['data'];
                var index = 0;
                for (; index < conferences.length; index++) {
                    var conf = conferences[index];
                    console.dir(conf);
                    this.items.push(conf);
                }
            }
            this.searchResults = this.items;
        }
    };
    UserpageComponent.prototype.logError = function (error) {
        console.error("Error in getAllConference() " + error);
    };
    UserpageComponent.prototype.ngOnInit = function () {
        //console.log(this.route.params['type']);
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            var type = params['type']; // (+) converts string 'id' to a number
            _this.updateSelectionForConference(type);
        });
    };
    UserpageComponent.prototype.updateSelectionForConference = function (value) {
        if (value === 'active') {
            this.currentSelection = "Active Conferences to Apply";
            this.current_status = 2;
            this.items = [];
            this.searchResults = [];
            this.getMyConferences(1);
        }
        else if (value === 'open') {
            this.currentSelection = "Open Conferences to Apply";
            this.current_status = 1;
            this.items = [];
            this.searchResults = [];
            this.getAllConferences();
        }
        else if (value == 'history') {
            this.currentSelection = "Conference History";
            this.current_status = 3;
            this.items = [];
            this.searchResults = [];
            this.getMyConferences(0);
        }
    };
    UserpageComponent.prototype.getStatusForConference = function (value) {
        if (value == 0) {
            return 'Closed';
        }
        else if (value == 1) {
            return 'Active';
        }
        else if (value == 2) {
            return 'Open';
        }
    };
    UserpageComponent.prototype.getMyConferences = function (state) {
        var _this = this;
        this.service.getMyConferences(this.service.getCurrentUser().id, state).subscribe(function (response) {
            // console.dir(response);
            _this.processResponse(response);
        }, function (error) {
            _this.logError(error);
        }, function () {
            console.log('Get All Conferences Complete');
        });
    };
    UserpageComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-userpage',
            templateUrl: 'userpage.component.html',
            styleUrls: ['userpage.component.css'],
            directives: [usermenu_component_1.UsermenuComponent, router_1.ROUTER_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [router_1.Router, router_1.ActivatedRoute, poc_service_service_1.PocServiceService])
    ], UserpageComponent);
    return UserpageComponent;
}());
exports.UserpageComponent = UserpageComponent;
//# sourceMappingURL=../../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/userpage/userpage.component.js.map