"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var UsermenuComponent = (function () {
    function UsermenuComponent(router) {
        this.router = router;
    }
    UsermenuComponent.prototype.ngOnInit = function () {
    };
    UsermenuComponent.prototype.doLogout = function () {
        localStorage.setItem('user_object', '');
        this.router.navigate(['landing']);
    };
    UsermenuComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-usermenu',
            templateUrl: 'usermenu.component.html',
            styleUrls: ['usermenu.component.css'],
            directives: [router_1.ROUTER_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [router_1.Router])
    ], UsermenuComponent);
    return UsermenuComponent;
}());
exports.UsermenuComponent = UsermenuComponent;
//# sourceMappingURL=../../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/usermenu/usermenu.component.js.map