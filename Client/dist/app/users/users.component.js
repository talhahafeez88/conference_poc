"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var poc_service_service_1 = require("../poc-service.service");
var UsersComponent = (function () {
    function UsersComponent(service) {
        this.service = service;
        this.items = [];
        this.searchResults = this.items;
        this.gender = ['Male', 'Female', "Other"];
        this.roles = ['Reviewer', 'Author'];
    }
    UsersComponent.prototype.ngOnInit = function () {
        console.log('onInit');
        this.getAllUsers();
    };
    UsersComponent.prototype.onKeyUp = function (value) {
        console.log('key up' + value);
        var results = [];
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].email.indexOf(value) == 0) {
                results.push(this.items[i]);
            }
        }
        this.searchResults = results;
    };
    UsersComponent.prototype.onRowClick = function () {
        //  alert('row clicked');
    };
    UsersComponent.prototype.getAllUsers = function () {
        var _this = this;
        this.service.getAllUsers().subscribe(function (response) {
            // console.dir(response);
            _this.processResponse(response);
        }, function (error) {
            _this.logError(error);
        }, function () {
            console.log('Get All Conferences Complete');
        });
    };
    UsersComponent.prototype.processResponse = function (response) {
        var confResponse = response;
        if (confResponse.success) {
            var index = 0;
            for (; index < confResponse.data.length; index++) {
                var conf = confResponse.data[index];
                console.dir(conf);
                this.items.push(conf);
            }
            this.searchResults = this.items;
        }
        else {
            this.searchResults = [];
        }
    };
    UsersComponent.prototype.logError = function (error) {
        console.error("Error in getAllConference() " + error);
    };
    UsersComponent.prototype.getGenderString = function (value) {
        if (value == '1') {
            return "Male";
        }
        else if (value == '2') {
            return 'Female';
        }
        else {
            return 'Other';
        }
    };
    UsersComponent.prototype.roleChanged = function (value, user_id) {
        var role_id = -1;
        if (value == 'Reviewer') {
            role_id = 1;
        }
        else {
            role_id = 2;
        }
        this.updateUserStatus(user_id, role_id);
        console.log('role changed' + value);
    };
    UsersComponent.prototype.getRoleString = function (value) {
        if (value == '1') {
            return "Reviewer";
        }
        else if (value == '2') {
            return 'Author';
        }
    };
    UsersComponent.prototype.updateRole = function (user_id) {
        var _this = this;
        var updatedUser = this.getUserWithUserId(user_id);
        console.dir(updatedUser);
        this.service.updateUser(updatedUser.id, updatedUser.status).subscribe(function (response) {
            console.dir(response);
        }, function (error) {
            _this.logError(error);
        }, function () {
            console.log('Get All Conferences Complete');
        });
    };
    UsersComponent.prototype.updateUserStatus = function (user_id, role_id) {
        for (var index = 0; index < this.searchResults.length; index++) {
            var user = this.searchResults[index];
            if (user.id == user_id) {
                user.status = role_id;
                break;
            }
        }
    };
    UsersComponent.prototype.getUserWithUserId = function (user_id) {
        for (var index = 0; index < this.searchResults.length; index++) {
            var user = this.searchResults[index];
            if (user.id == user_id) {
                return user;
            }
        }
    };
    UsersComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-users',
            templateUrl: 'users.component.html',
            styleUrls: ['users.component.css'],
            directives: [router_1.ROUTER_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [poc_service_service_1.PocServiceService])
    ], UsersComponent);
    return UsersComponent;
}());
exports.UsersComponent = UsersComponent;
//# sourceMappingURL=../../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/users/users.component.js.map