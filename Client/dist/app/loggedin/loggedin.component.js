///<reference path="../../../typings/globals/jquery/index.d.ts" />
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var LoggedinComponent = (function () {
    function LoggedinComponent(router) {
        this.items = ['1', '2', '3', '4'];
        this.searchResults = this.items;
        this.router = router;
    }
    LoggedinComponent.prototype.ngOnInit = function () {
        console.log('onInit');
        var me = true;
        // jQuery(this.elementRef.nativeElement.contains('#grid-basic')).bootgrid();
    };
    LoggedinComponent.prototype.onKeyUp = function (value) {
        console.log('key up' + value);
        var results = [];
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].indexOf(value) == 0) {
                results.push(this.items[i]);
            }
        }
        this.searchResults = results;
    };
    LoggedinComponent.prototype.onRowClick = function () {
        alert('row clicked');
    };
    LoggedinComponent.prototype.doLogout = function () {
        localStorage.setItem('user_object', '');
        this.router.navigate(['landing']);
    };
    LoggedinComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-loggedin',
            templateUrl: 'loggedin.component.html',
            styleUrls: ['loggedin.component.css'],
            directives: [router_1.ROUTER_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [router_1.Router])
    ], LoggedinComponent);
    return LoggedinComponent;
}());
exports.LoggedinComponent = LoggedinComponent;
//# sourceMappingURL=../../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/loggedin/loggedin.component.js.map