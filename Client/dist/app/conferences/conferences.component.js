"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var loggedin_component_1 = require("../loggedin/loggedin.component");
var poc_service_service_1 = require("../poc-service.service");
var ConferencesComponent = (function () {
    function ConferencesComponent(router, service) {
        this.service = service;
        this.router = router;
        this.items = [];
        this.searchResults = [];
        this.getAllConferences();
    }
    ConferencesComponent.prototype.ngOnInit = function () {
        // jQuery(this.elementRef.nativeElement.contains('#grid-basic')).bootgrid();
    };
    ConferencesComponent.prototype.onKeyUp = function (value) {
        console.log('key up' + value);
        var results = [];
        for (var i = 0; i < this.items.length; i++) {
            var conf = this.items[i];
            if (conf['name'].indexOf(value) == 0) {
                results.push(this.items[i]);
            }
        }
        this.searchResults = results;
    };
    ConferencesComponent.prototype.onRowClick = function (user) {
        var conf = this.searchResults[user.rowIndex - 1];
        console.dir(conf);
        this.router.navigate(['conferencedetails', conf['id']]);
    };
    ConferencesComponent.prototype.onAddConferenceClick = function () {
        this.router.navigate(['addconference']);
    };
    ConferencesComponent.prototype.getAllConferences = function () {
        var _this = this;
        this.service.getAllConferencesAdmin().subscribe(function (response) {
            // console.dir(response);
            _this.processResponse(response);
        }, function (error) {
            _this.logError(error);
        }, function () {
            console.log('Get All Conferences Complete');
        });
    };
    ConferencesComponent.prototype.processResponse = function (response) {
        var confResponse = response;
        if (confResponse.success) {
            var index = 0;
            for (; index < confResponse.data.length; index++) {
                var conf = confResponse.data[index];
                console.dir(conf);
                this.items.push(conf);
            }
            this.searchResults = this.items;
        }
        else {
            this.searchResults = [];
        }
    };
    ConferencesComponent.prototype.logError = function (error) {
        console.error("Error in getAllConference() " + error);
    };
    ConferencesComponent.prototype.getStatusForConference = function (value) {
        if (value == 0) {
            return 'Closed';
        }
        else if (value == 1) {
            return 'Active';
        }
        else if (value == 2) {
            return 'Open';
        }
    };
    ConferencesComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-conferences',
            templateUrl: 'conferences.component.html',
            styleUrls: ['conferences.component.css'],
            directives: [router_1.RouterLink, router_1.ROUTER_DIRECTIVES, loggedin_component_1.LoggedinComponent],
            providers: [poc_service_service_1.PocServiceService]
        }), 
        __metadata('design:paramtypes', [router_1.Router, poc_service_service_1.PocServiceService])
    ], ConferencesComponent);
    return ConferencesComponent;
}());
exports.ConferencesComponent = ConferencesComponent;
//# sourceMappingURL=../../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/conferences/conferences.component.js.map