"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var login_component_1 = require("../login/login.component");
var index_1 = require('../models/index');
var poc_service_service_1 = require("../poc-service.service");
require('rxjs/add/operator/map');
require('rxjs/add/operator/toPromise');
var SignupComponent = (function () {
    function SignupComponent(router, service) {
        this.router = router;
        this.genders = ['Male', 'Female', 'Other'];
        this.roles = ['Reviewer', 'Author'];
        this.email_error = true;
        this.suser = new index_1.Users("", "");
        this.service = service;
        this.suser.gender = 1;
        this.suser.role_id = 1;
        //    this.setupUserForSignup();
    }
    SignupComponent.prototype.setupUserForSignup = function () {
        this.suser.first_name = "Asad";
        this.suser.last_name = "Rehman";
        this.suser.age = 29;
        this.suser.gender = 1;
        this.suser.email = "asad@one.com";
        this.suser.password = "root123";
        this.suser.role_id = 1;
    };
    SignupComponent.prototype.ngOnInit = function () {
    };
    SignupComponent.prototype.doSignup = function (f) {
        var _this = this;
        console.log(f);
        this.email_error = false;
        this.email_error = true;
        var signupJson = this.getFormJSON();
        var result = this.service.doSignup(signupJson).
            subscribe(function (response) {
            console.dir(response);
            _this.processSignupResults(response);
        }, function (error) {
            _this.errorInSignup(error);
        }, function () {
            console.log('Signup Complete');
        });
    };
    SignupComponent.prototype.processSignupResults = function (response) {
        if (!response['success']) {
            this.email_error = false;
        }
        else {
            this.email_error = false;
            var responseUser = response.data[0];
            if (responseUser.role_id == 0) {
            }
            else if (responseUser.role_id == 1) {
                this.doSignIn(this.suser.email, this.suser.password);
            }
        }
    };
    SignupComponent.prototype.roleChanged = function (value) {
        if (value == 'Reviewer') {
            this.suser.role_id = 1;
        }
        else {
            this.suser.role_id = 2;
        }
        console.log('role changed' + value);
    };
    SignupComponent.prototype.genderChanged = function (value) {
        if (value == 'Reviewer') {
            this.suser.role_id = 1;
        }
        else {
            this.suser.role_id = 2;
        }
        console.log('role changed' + value);
    };
    SignupComponent.prototype.getFormJSON = function () {
        var me = this;
        var signupForm = {
            "first_name": me.suser.first_name,
            "last_name": me.suser.last_name,
            "age": me.suser.age,
            "gender": me.suser.gender,
            "email": me.suser.email,
            "password": me.suser.password,
            "role_id": me.suser.role_id
        };
        console.log(signupForm);
        return signupForm;
    };
    SignupComponent.prototype.errorInSignup = function (response) {
        alert('An Error occured during signup. Please try again later');
    };
    SignupComponent.prototype.doSignIn = function (user, password) {
        var _this = this;
        this.service.doSignIn(user, password).subscribe(function (response) {
            // console.dir(response);
            _this.processSignInResponse(response);
        }, function (error) {
            _this.errorInSignup(error);
        }, function () {
            console.log('SignIn Complete');
        });
    };
    SignupComponent.prototype.processSignInResponse = function (response) {
        console.dir(response);
        var responseUser = null;
        if (!response['success']) {
        }
        else {
            responseUser = response.data;
            localStorage.setItem('user_object', JSON.stringify(responseUser));
            if (responseUser.role_id == 0) {
                //user is admin
                this.router.navigate(['adminpage']);
            }
            else if (responseUser.role_id == 1) {
                this.router.navigate(['userpage']);
            }
        }
    };
    SignupComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-signup',
            templateUrl: 'signup.component.html',
            styleUrls: ['signup.component.css'],
            directives: [router_1.ROUTER_DIRECTIVES, login_component_1.LoginComponent],
            providers: [poc_service_service_1.PocServiceService]
        }), 
        __metadata('design:paramtypes', [router_1.Router, poc_service_service_1.PocServiceService])
    ], SignupComponent);
    return SignupComponent;
}());
exports.SignupComponent = SignupComponent;
//# sourceMappingURL=../../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/signup/signup.component.js.map