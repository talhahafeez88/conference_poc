"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
require('rxjs/add/operator/toPromise');
var PocServiceService = (function () {
    function PocServiceService(http) {
        this._baseURL = "http://localhost:3000";
        this._http = http;
        var user = localStorage.getItem('user_object');
        if (user != null) {
            this.loggedIn = true;
        }
        else {
            this.loggedIn = false;
        }
    }
    PocServiceService.prototype.doSignIn = function (email, password) {
        console.log('signin request result is ' + email + password);
        return this.processSignIn(email, password);
    };
    PocServiceService.prototype.doSignup = function (value) {
        return this.processSignup(value);
    };
    PocServiceService.prototype.processSignIn = function (email, password) {
        console.log('Web Service Call');
        var username = email;
        var password = password;
        var url = this._baseURL + '/signin';
        var creds = "email=" + username + "&password=" + password;
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this._http.post(url, creds, {
            headers: headers
        })
            .map(function (res) { return res.json(); });
    };
    PocServiceService.prototype.processSignup = function (signupForm) {
        console.log('Web Service Call');
        var url = this._baseURL + '/register';
        console.log(url);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        console.log(signupForm);
        return this._http.post(url, signupForm, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    PocServiceService.prototype.getAllConferences = function () {
        console.log('Web Service Call');
        var url = this._baseURL + '/getAllConferences';
        console.log(url);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var user_id = this.getCurrentUser().id;
        return this._http.post(url, { "user_id": user_id }, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    PocServiceService.prototype.createNewConference = function (value) {
        console.log('Web Service Call ' + JSON.stringify(value));
        var url = this._baseURL + '/createConference';
        console.log(url);
        var headers = new http_1.Headers();
        var conference = JSON.stringify(value);
        headers.append('Content-Type', 'application/json');
        return this._http.post(url, conference, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    //getAllConferencesAdmin
    PocServiceService.prototype.getAllConferencesAdmin = function () {
        console.log('Web Service Call');
        var url = this._baseURL + '/getAllConferencesAdmin';
        console.log(url);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var user_id = this.getCurrentUser().id;
        return this._http.post(url, { "user_id": user_id }, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    PocServiceService.prototype.getConferenceDetails = function (conf_id) {
        ///getConferenceDetails
        console.log('Web Service Call');
        var url = this._baseURL + '/getConferenceDetails';
        console.log(url);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(url, { "conf_id": conf_id }, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    PocServiceService.prototype.joinConference = function (conf_id, user_id) {
        console.log('Web Service Call');
        var url = this._baseURL + '/joinConference';
        console.log(url);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(url, {
            "conference_id": conf_id,
            "user_id": user_id
        }, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    PocServiceService.prototype.getMyConferences = function (user_id, state) {
        console.log('Web Service Call');
        var url = this._baseURL + '/getMyConference';
        console.log(url);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var user_ids = this.getCurrentUser().id;
        return this._http.post(url, { "user_id": user_ids,
            "state": state
        }, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    PocServiceService.prototype.submitDocument = function (document) {
        console.log('Web Service Call');
        var url = this._baseURL + '/submitDocument';
        console.log(url);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        //alert();
        console.dir(JSON.stringify(document));
        return this._http.post(url, document, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    PocServiceService.prototype.getDocumentsForConference = function (conf_id) {
        console.log('Web Service Call');
        var url = this._baseURL + '/getConfrenceDetailsForUser';
        console.log(url);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var user_id = this.getCurrentUser().id;
        return this._http.post(url, { "conference_id": conf_id,
            "user_id": user_id
        }, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    PocServiceService.prototype.getAllUsers = function () {
        console.log('Web Service Call');
        var url = this._baseURL + '/getAllUsers';
        console.log(url);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(url, {}, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    PocServiceService.prototype.saveJwt = function (jwt) {
        if (jwt) {
            console.dir('Error occured with request ' + jwt);
        }
    };
    PocServiceService.prototype.getCurrentUser = function () {
        var rootUser = localStorage.getItem('user_object');
        if (!rootUser) {
            return;
        }
        var user = JSON.parse(rootUser);
        console.dir(user);
        if (user.id > 0) {
            return user;
        }
        return null;
    };
    PocServiceService.prototype.updateUser = function (user_id, status_id) {
        console.log('Web Service Call');
        var url = this._baseURL + '/updateUser';
        console.log(url);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(url, {
            "status_id": status_id,
            "user_id": user_id
        }, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    PocServiceService.prototype.getAllReviews = function (is_active, user_id) {
        console.log('Web Service Call');
        var url = this._baseURL + '/getAllReviews';
        console.log(url);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        //var user_id = this.getCurrentUser().id;
        var is_active_string = null;
        if (is_active == 0) {
            is_active_string = "false";
        }
        else {
            is_active_string = "true";
        }
        console.log({
            "is_active": is_active_string,
            "user_id": user_id
        });
        return this._http.post(url, {
            "is_active": is_active_string,
            "user_id": user_id
        }, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    PocServiceService.prototype.submitReview = function (json) {
        console.log('Web Service Call');
        var url = this._baseURL + '/submitReview';
        console.log(url);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        //alert();
        // console.dir(JSON.stringify(document));
        return this._http.post(url, json, {
            headers: headers
        })
            .map(function (response) { return response.json(); });
    };
    PocServiceService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], PocServiceService);
    return PocServiceService;
}());
exports.PocServiceService = PocServiceService;
//# sourceMappingURL=../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/poc-service.service.js.map