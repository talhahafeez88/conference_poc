"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var poc_service_service_1 = require("./poc-service.service");
var AuthorizationService = (function () {
    function AuthorizationService(http, service) {
        this.http = http;
        this.service = service;
        this.loggedIn = false;
        this.login_error = false;
        this.loggedIn = !!localStorage.getItem('is_logged_in');
    }
    AuthorizationService.prototype.login = function (email, password) {
        var _this = this;
        this.service.doSignIn(email, password).subscribe(function (response) {
            // console.dir(response);
            _this.processSignInResponse(response);
        }, function (error) {
            _this.errorInSignup(error);
        }, function () {
            console.log('SignIn Complete');
        });
    };
    AuthorizationService.prototype.processSignInResponse = function (response) {
        console.dir(response);
        var responseUser = null;
        if (!response['success']) {
            this.login_error = false;
        }
        else {
            responseUser = response.data;
            if (responseUser.role_id == 0) {
            }
            else if (responseUser.role_id == 1) {
            }
            this.login_error = true;
        }
    };
    AuthorizationService.prototype.errorInSignup = function (response) {
        alert('An Error occured during Signin. Please try again later');
    };
    AuthorizationService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, poc_service_service_1.PocServiceService])
    ], AuthorizationService);
    return AuthorizationService;
}());
exports.AuthorizationService = AuthorizationService;
//# sourceMappingURL=../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/authorization.service.js.map