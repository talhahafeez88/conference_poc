"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var loggedin_component_1 = require("../loggedin/loggedin.component");
var index_1 = require("../models/index");
var poc_service_service_1 = require("../poc-service.service");
var AddconferenceComponent = (function () {
    function AddconferenceComponent(router, service) {
        this.service = service;
        this.statuses = ['Active', 'Open'];
        this.form_error = true;
        this.router = router;
        this.conference = new index_1.Conference();
        this.conference.status = 1;
    }
    AddconferenceComponent.prototype.ngOnInit = function () {
    };
    AddconferenceComponent.prototype.createConference = function () {
        var _this = this;
        this.form_error = false;
        this.service.createNewConference(this.conference).subscribe(function (response) {
            console.dir(response);
            _this.processResponse(response);
        }, function (error) {
            _this.logError(error);
        }, function () {
            console.log('SignIn Complete');
        });
    };
    AddconferenceComponent.prototype.processResponse = function (respone) {
        var confResponse = respone;
        if (confResponse.success) {
            this.form_error = true;
            //          alert('Conference created Successfully');
            this.router.navigate(['conferences']);
        }
        else {
            this.form_error = false;
        }
    };
    AddconferenceComponent.prototype.logError = function (error) {
    };
    AddconferenceComponent.prototype.statusChanged = function (value) {
        if (value == 'Active') {
            this.conference.status = 1;
        }
        else {
            this.conference.status = 2;
        }
    };
    AddconferenceComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-addconference',
            templateUrl: 'addconference.component.html',
            styleUrls: ['addconference.component.css'],
            directives: [router_1.RouterLink, router_1.ROUTER_DIRECTIVES, loggedin_component_1.LoggedinComponent],
            providers: [poc_service_service_1.PocServiceService]
        }), 
        __metadata('design:paramtypes', [router_1.Router, poc_service_service_1.PocServiceService])
    ], AddconferenceComponent);
    return AddconferenceComponent;
}());
exports.AddconferenceComponent = AddconferenceComponent;
//# sourceMappingURL=../../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/addconference/addconference.component.js.map