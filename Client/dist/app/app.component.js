"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var config_service_1 = require('./config.service');
var router_1 = require('@angular/router');
var login_component_1 = require("./login/login.component");
var landing_component_1 = require("./landing/landing.component");
var footer_component_1 = require("./footer/footer.component");
var signup_component_1 = require("./signup/signup.component");
var loggedin_component_1 = require("./loggedin/loggedin.component");
var poc_service_service_1 = require("./poc-service.service");
var AppComponent = (function () {
    function AppComponent(router, service) {
        this.service = service;
        this.title = config_service_1.Config.APPLICATION_NAME;
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        var user = this.service.getCurrentUser();
        if (!user) {
            this.router.navigate(['landing']);
        }
        else {
            if (user.id > 0) {
                if (user.role_id == 0) {
                    this.router.navigate(['adminpage']);
                }
                else {
                    this.router.navigate(['userpage']);
                }
            }
            else {
                this.router.navigate(['landing']);
            }
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-root',
            templateUrl: 'app.component.html',
            styleUrls: ['app.component.css'],
            directives: [login_component_1.LoginComponent, landing_component_1.LandingComponent, footer_component_1.FooterComponent, signup_component_1.SignupComponent, loggedin_component_1.LoggedinComponent, router_1.ROUTER_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [router_1.Router, poc_service_service_1.PocServiceService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/app.component.js.map