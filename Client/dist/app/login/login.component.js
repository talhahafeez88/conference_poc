"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var config_service_1 = require('../config.service');
var router_1 = require('@angular/router');
var index_1 = require('../models/index');
var poc_service_service_1 = require("../poc-service.service");
var LoginComponent = (function () {
    function LoginComponent(router, service) {
        this.login_error = true;
        this.title = config_service_1.Config.APPLICATION_NAME;
        this.router = router;
        this.service = service;
        this.login_error = true;
        this.user = new index_1.Users("", "");
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.doSignIn = function (f) {
        var _this = this;
        this.login_error = false;
        this.service.doSignIn(this.user.email, this.user.password).subscribe(function (response) {
            // console.dir(response);
            _this.processSignInResponse(response);
        }, function (error) {
            _this.errorInSignup(error);
        }, function () {
            console.log('SignIn Complete');
        });
    };
    LoginComponent.prototype.processSignInResponse = function (response) {
        console.dir(response);
        var responseUser = null;
        if (!response['success']) {
            this.login_error = false;
        }
        else {
            responseUser = response.data;
            localStorage.setItem('user_object', JSON.stringify(responseUser));
            if (responseUser.role_id == 0) {
                //user is admin
                this.router.navigate(['adminpage']);
            }
            else if (responseUser.role_id == 1) {
                this.router.navigate(['userpage']);
            }
            this.login_error = true;
        }
    };
    LoginComponent.prototype.doSignup = function () {
        this.router.navigate(['signup']);
    };
    LoginComponent.prototype.doLogout = function () {
        this.router.navigate(['defaultroute']);
    };
    LoginComponent.prototype.errorInSignup = function (response) {
        alert('An Error occured during Signin. Please try again later');
    };
    LoginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-login',
            templateUrl: 'login.component.html',
            styleUrls: ['login.component.css'],
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [poc_service_service_1.PocServiceService]
        }), 
        __metadata('design:paramtypes', [router_1.Router, poc_service_service_1.PocServiceService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=../../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/login/login.component.js.map