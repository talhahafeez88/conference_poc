"use strict";
/**
 * Created by apple on 25/07/2016.
 */
var router_1 = require('@angular/router');
var signup_component_1 = require("./signup/signup.component");
var landing_component_1 = require("./landing/landing.component");
var loggedin_component_1 = require("./loggedin/loggedin.component");
var conferences_component_1 = require("./conferences/conferences.component");
var users_component_1 = require("./users/users.component");
var addconference_component_1 = require("./addconference/addconference.component");
var conferencedetails_component_1 = require("./conferencedetails/conferencedetails.component");
var admin_component_1 = require("./admin/admin.component");
var app_component_1 = require("./app.component");
var userpage_component_1 = require("./userpage/userpage.component");
var submit_component_1 = require("./submit/submit.component");
var auth_guard_service_1 = require("./auth-guard.service");
var reviews_component_1 = require("./reviews/reviews.component");
var routes = [
    {
        path: '',
        component: app_component_1.AppComponent,
    },
    { path: 'signup', component: signup_component_1.SignupComponent },
    { path: 'landing', component: landing_component_1.LandingComponent },
    { path: 'loggedin', component: loggedin_component_1.LoggedinComponent },
    { path: 'conferences', component: conferences_component_1.ConferencesComponent },
    { path: 'users', component: users_component_1.UsersComponent },
    { path: 'addconference', component: addconference_component_1.AddconferenceComponent },
    { path: 'conferencedetails/:conference', component: conferencedetails_component_1.ConferencedetailsComponent },
    { path: 'adminpage', component: admin_component_1.AdminComponent },
    { path: 'userpage', component: userpage_component_1.UserpageComponent, canActivate: [auth_guard_service_1.AuthGuardService] },
    { path: 'submit/:conf_id', component: submit_component_1.SubmitComponent },
    { path: 'userpage/:type', component: userpage_component_1.UserpageComponent },
    {
        path: 'defaultroute',
        component: app_component_1.AppComponent
    },
    { path: 'conferencedetails/:conference/:isActive/:isOpen/:isUser', component: conferencedetails_component_1.ConferencedetailsComponent, canActivate: [auth_guard_service_1.AuthGuardService] },
    { path: 'reviews/:type', component: reviews_component_1.ReviewsComponent },
];
exports.appRouterProviders = [
    router_1.provideRouter(routes)
];
//# sourceMappingURL=../tmp/broccoli_type_script_compiler-input_base_path-PLSmbvCi.tmp/0/src/app/app.route.js.map