-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 10, 2016 at 07:37 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `conference_poc_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `conference_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `doc_id` int(11) DEFAULT NULL,
  `comments` varchar(256) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `conference_id`, `user_id`, `doc_id`, `comments`, `role_id`) VALUES
(1, 4, 1, 1, 'kjaskdjaskjdkfj', 1),
(2, 4, 2, 2, 'sdfadsf', 1),
(3, 4, 3, 3, 'sdmnfamsd', 1),
(4, 4, 4, 4, 'asdfasd', 1);

-- --------------------------------------------------------

--
-- Table structure for table `conference`
--

CREATE TABLE `conference` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `review_days` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conference`
--

INSERT INTO `conference` (`id`, `name`, `description`, `start_date`, `end_date`, `review_days`, `status`) VALUES
(1, 'Confrence 1', 'Confrence 1', '2016-08-10 00:00:00', '2016-08-08 00:00:00', 3, 1),
(2, 'Confrence 2', 'Confrence 2', '2016-08-10 00:00:00', '2016-08-08 00:00:00', 4, 1),
(3, 'Confrence 3', 'Confrence 3', '2016-08-10 00:00:00', '2016-08-08 00:00:00', 2, 1),
(4, 'Confrence 4', 'Confrence 4', '2016-08-10 00:00:00', '2016-08-08 00:00:00', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(11) NOT NULL,
  `path` varchar(128) DEFAULT NULL,
  `documentName` varchar(45) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `path`, `documentName`, `description`) VALUES
(1, '/var/folder/umaid', 'UmaidDocument', 'UmaidDocument'),
(2, '/var/folder/usman', 'UsmanDocument', 'UsmanDocument'),
(3, '/var/folder/waseem', 'WaseemDocument', 'WaseemDocument'),
(4, '/var/folder/abdullah', 'AbdulahDocument', 'AbdullaDocument'),
(7, 'var/html/hassan', 'hassan Document', 'hello helllo helllo'),
(8, 'undefined', 'undefined', 'undefined'),
(9, 'undefined', 'undefined', 'undefined'),
(10, 'undefined', 'undefined', 'undefined'),
(11, 'undefined', 'undefined', 'undefined'),
(12, 'undefined', 'undefined', 'undefined'),
(13, 'undefined', 'undefined', 'undefined'),
(14, 'undefined', 'undefined', 'undefined'),
(15, 'http://localhost:3000/uploads/upload-1470848440556.jpeg', '34871530E2004ABBCCB16886D39AD1C27EF86CDFF384B', 'xczxcx'),
(16, 'http://localhost:3000/uploads/upload-1470849022208.png', 'Add customer details story 1.png', 'asad'),
(17, 'http://localhost:3000/uploads/upload-1470849104326.docx', '4706application.docx', 'asad');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `doc_id` int(11) DEFAULT NULL,
  `comments` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `activity_id`, `user_id`, `doc_id`, `comments`) VALUES
(1, 1, 4, 17, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `age`, `gender`, `email`, `password`, `role_id`, `status`) VALUES
(1, 'Review', 'Admin', 28, 1, 'admin@poc.com', 'root123', 0, 0),
(2, 'Umaid', 'Naeem', 28, 1, 'asad@one.com', 'root123', 1, 1),
(3, 'Usman', 'Noor', 28, 1, 'usman@example.com', 'usman', 1, 1),
(4, 'Waseem', 'Imran', 28, 1, 'waseem@example.com', 'waseem', 1, 1),
(5, 'Abdullah', 'Salman', 28, 1, 'adbullah@example.com', 'Abdullah', 1, 1),
(6, 'Asad', 'Rehman', 213, 1, 'asad2@one.com', 'asad', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_conference`
--

CREATE TABLE `user_conference` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `conference_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_conference`
--

INSERT INTO `user_conference` (`id`, `users_id`, `conference_id`) VALUES
(1, 1, 4),
(2, 2, 4),
(3, 3, 4),
(4, 4, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conference`
--
ALTER TABLE `conference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- Indexes for table `user_conference`
--
ALTER TABLE `user_conference`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `conference`
--
ALTER TABLE `conference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_conference`
--
ALTER TABLE `user_conference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
